<?php get_header(); ?>


<section class="basic-header white-text">
   <div class="wrapper">
      <h1 class="center"><?php the_title(); ?></h1>

       <div class="clear"></div>
   </div>
</section>

<section class="basicpage-main-content">
   <div class="wrapper"><br><br>

			

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<?php // comments_template( '', true ); // Remove if you don't want comments ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

   <div class="clear"></div>
   </div>
</section>   
<?php get_footer(); ?>
