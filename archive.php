<?php get_header(); ?>
<!-- section -->

<section class="basic-header white-text">
   <div class="wrapper center"><br>
 <h1 class="center">Archive</h1>

   </div>
</section>
<?php 
        $category = get_the_category();
         wp_reset_postdata(); 
?>        
<section class="search-results-main">
   <div class="wrapper">

			<p class="size20 center"><?php _e( 'Archives', 'html5blank' ); ?></p>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

 <div class="clear"></div>
   </div>
</section>   


<?php get_footer(); ?>
