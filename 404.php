<?php get_header(); ?>
<?php get_header(); ?>


<section class="basic-header white-text">
   <div class="wrapper">
      <h1 class="center">Error 404 Page not found</h1>

       <div class="clear"></div>
   </div>
</section>

<section class="basicpage-main-content">
   <div class="wrapper"><br><br>

			<!-- article -->
			<article id="post-404">

				
				<h2>
					<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
				</h2>

			</article>
			<!-- /article -->

   <div class="clear"></div>
   </div>
</section>   
<?php get_footer(); ?>
