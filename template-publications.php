<?php  /* Template Name: Publications Page Template */ 

//if (!is_user_logged_in() ) wp_redirect( site_url( 'home' ) );
get_header(); 
 $current_user = wp_get_current_user();
?>
<!-- section -->

<section class="basic-header white-text">
   <div class="wrapper center"><br>
 <h1 class="center">Publications</h1>
 <form class="search " method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-input1" type="search" name="s" placeholder="<?php _e( 'Search Publications', 'html5blank' ); ?>">
	<input type="hidden" name="post_types" value="post" />
	<button class="search-submit button primary" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
</form>

   </div>
</section>
<section class="publications-menu">
    <div class="wrapper">
      <ul class="p-menu">
          <li><a class="pbb" href="/category/blog/"><span>Blogs</span></a></li>
          <li><a class="pbp" href="/category/press-releases/"><span>Press releases</span></a></li>
          <li><a class="pbr" href="/category/whitepapers/"><span>Reports</span></a></li>
          <li><a class="pbv" href="/category/videos/"><span>Videos</span></a></li>
          <li><a class="pbw" href="/category/webinars/"><span>Webinars</span></a></li>
      </ul>
      <div class="clear"></div>
   </div>
</section>

<section class="publications-reports-blogs">
   <div class="wrapper">
   
   <div class="one-half">
  
        <h2>Reports</h2>
      <?php
      $i=0;
         $args = array( 'posts_per_page' => 3, 'category' => 98 );
         
         $myposts = get_posts( $args );
         foreach ( $myposts as $post ) : setup_postdata( $post ); 
         print "<div class=\"div$i pb-reports-container\"><div class='reports-thumbnail'>";
         $i++; 
         
         if (str_word_count (get_the_title())<5)$long_excerpt=true; else $long_excerpt=false;
         if (str_word_count (get_the_title())>10)$no_content=true; else $no_content=false;
         
         
          if ( has_post_thumbnail() ) {
         the_post_thumbnail( 'reports' );
} 
          ?></div>
          <div class="blog-content-right">
            <div class="report-indicator">Report</div>
            <div class=" blog-heading-div"><a class="news-heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
            <?php if ($long_excerpt==true) { ?> <div class="blogs-content"><?php html5wp_excerpt(20); ?></div> <?php } ?>

            <?php if ($long_excerpt==false && $no_content==false) { ?> <div class="blogs-content"><?php html5wp_excerpt(10); ?></div> <?php } ?>
            <div class="blogs-date"><?php print get_the_date('jS \o\f F Y'); ?>
                  <div class="blog-link"><a class="read-more" href="<?php the_permalink(); ?>">Read more</a></div>
        </div>
      </div>
   </div>
   <div class="clear"></div>
   <?php endforeach; 
      wp_reset_postdata();?>
   </div>
  
   
  <div class="second-half">
      <h2>Blogs</h2>
      <?php
      $i=1;
         $args = array( 'posts_per_page' => 2, 'category' => 4 );
         
         $myposts = get_posts( $args );
         foreach ( $myposts as $post ) : setup_postdata( $post ); 
         print "<div class=\"div$i blogs-container\">";
         $i++;
         
             if ( has_post_thumbnail() ) {
             print "<div class=\"blog-thumb\">";
         the_post_thumbnail( 'reports' );
             print "</div>";
} 
          ?>
        <div class="blog-content-right">
          <div class="blog-indicator">blog</div>
          <div class=" blog-heading-div"><a class="news-heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
          <div class="blogs-author"><a href="/category/blog/">Mobey Forum's Blog</a></div>

          <div class="blogs-content"><?php html5wp_excerpt(20); ?></div>
          <div class="blogs-date"><?php print get_the_date('jS \o\f F Y'); ?>
          <div class="blog-link"><a class="read-more" href="<?php the_permalink(); ?>">Read more</a></div>
        </div>
      </div>
   </div>
   <div class="clear"></div>
   <?php endforeach; 
      wp_reset_postdata();?>
   </div>
   <div class="clear"></div>
   </div> 
      
</section>
<section class="publications-videos-webinars">
   <div class="wrapper">
      <div class="one-half">
      <h2>Videos</h2>
      
      
          
      <?php
      $i=0;
         $args = array( 'posts_per_page' => 3,  'post_type' => 'Videos' );
         
         $myposts = get_posts( $args );
         foreach ( $myposts as $post ) : setup_postdata( $post ); 
         print "<div class=\"vw-container\"><div class='video-thumbnail'>";
         $i++; 
         $custom = get_post_custom($post->ID);
          $youtube_url= $custom["youtube-url"][0];
         print "<a class=\"read-more\" target=\"_blank\" href=\"$youtube_url\"><div class=\"play\"></div>";
         
          if ( has_post_thumbnail() ) {
         the_post_thumbnail( 'medium' );
} 
          ?></a></div><div class="content">
        <div class="video-indicator">Video</div>
        <div class="heading-div-db"><span class="news-heading"><?php the_title(); ?></span></div>
        <div class="blogs-date"><?php print get_the_date('jS \o\f F Y'); ?>
        <div class="blog-link"><a class="read-more" target="_blank" href="<?php print $youtube_url; ?>">Watch now</a></div>
        </div>
        
      </div>
      
   </div>
   
   <?php endforeach; 
      wp_reset_postdata();?>
      
        
      </div>
      <div class="second-half">
      <h2>Webinars</h2>
      
      <?php
           $i=0;
            $args = array( 'posts_per_page' => 3, 'category' => 149);
            
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post ); 
            $custom = get_post_custom($post->ID);
           $alkaa = $custom["alkaa_pv"][0];
      $alkaa_klo= $custom["alkaa_klo"][0];
      $loppuu= $custom["loppuu_pv"][0];
      $loppuu_klo= $custom["loppuu_klo"][0];
      $registration_link= $custom["registration_link"][0];
       $youtube_id= $custom["paikka"][0];
        $indeksi = strtotime($alkaa." 15:00:$i");
        $today = strtotime("now");
        $pvm = strtotime($alkaa." ".$alkaa_klo);
        $notification="";
        if ($today < $pvm) $future=true; else $future=false;
          
        if ($future == true)  $webinars[$indeksi] .= "<div class=\"vw-container future\"><div class=\"w-dot\"></div><div class=\"w-content\">";
        else $webinars[$indeksi] .= "<div class=\"vw-container \"><div class=\"w-dot\"></div><div class=\"w-content\">";
            
         if ($future == true) $webinars[$indeksi] .= '<div class="inactive-indicator">Upcoming Webinar</div>';
         else $webinars[$indeksi] .= '<div class="webinar-indicator">Webinar</div>';
         $webinars[$indeksi] .= '<div class="heading-div-db"><a class="news-heading size14" href="'.get_the_permalink().'">'.get_the_title().'</a></div>';
        
         $webinars[$indeksi] .= '<div class="blogs-date left">'.date('jS \o\f F Y', $pvm).'</div>';
       if ($youtube_id)  $webinars[$indeksi] .= '<div class="download-link"><a class="right" href="'.get_the_permalink().'">Watch now</a></div>';
        else$webinars[$indeksi] .= '<div class="download-link right"><a class="read-more" href="'.get_the_permalink().'">Read more</a></div>';
         $webinars[$indeksi] .= ' <div class="clear"></div></div></div>';
      endforeach; 
         wp_reset_postdata();
         $i=0;
       
         krsort($webinars);
         foreach ($webinars as $key => $val) {
         $i++;
          print $val;
          if ($i==2) break;
          
          }
          ?>

  
  
      </div>
       <div class="clear"></div>
   </div>       
</section>
<section class="publications-press-releases">
   <div class="wrapper">
   <h2>Press Releases</h2>
    <?php
           $i=0;
            $args = array( 'posts_per_page' => 3, 'category' => 7 );
            
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post ); 
            print "<div class=\"div$i pbpress-container\">";
            $i++;
            ?>
         <div class="heading-div"><a class="news-heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
         <div class="news-date"><?php print get_the_date('jS \o\f F Y'); ?></div>
         <div class="news-link"><a class="read-more" href="<?php the_permalink(); ?>">Read more</a></div>
      </div>
      <?php endforeach; 
         wp_reset_postdata();?>
         <div class="clear"></div>
      </div>       
</section>
<?php get_footer(); ?>

