<?php  /* Template Name: Join Page Template */ 

//if (!is_user_logged_in() ) wp_redirect( site_url( 'home' ) );
get_header(); 
 $current_user = wp_get_current_user();
$mobey_workgroups =  get_option('mobey_workgroups'); 

?>

<!-- section -->

<section class="basic-header white-text">
   <div class="wrapper">
      <h1 class="center">Join</h1>

       <div class="clear"></div>
   </div>
</section>
<section class="basicpage-main-content join-main">
   <div class="wrapper center">
   
    <h2>Are you a &nbsp;&nbsp;<img src="<?php echo get_template_directory_uri(); ?>/img/mobey-forum-diamond.png" alt="Mobey Forum" class="middle"> &nbsp;&nbsp;member yet?</h2>
    <p class="size20">
    Mobey Forum membership is open to <b>all the future-oriented banks</b> and <b>financial companies</b> seeking for new business opportunities and successful ideas.
    </p><br>
    <div class="clear"><a class="button primary inline" target="_blank" href="https://www.mobeyforum.org/wp-content/uploads/2017/09/Mobey-Forum-Membership-Application-Form.docx">Application Form</a>
   <a class="button secondary inline" href="https://www.mobeyforum.org/about/">Learn more</a></div>
      <a class="button secondary inline" href="mailto:elina.mattila@mobeyforum.org">Contact Us</a></div>

    </div>
          
</section>

<section class="join-benefits">
  <div class="wrapper center">

      <h2>Being part of Mobey, you:</h2>
      
      <div class="being-part-container"> 
        <div class="bpc bpc1">
        Meet the industry friends in <span class="bold">an open and honest environment</span>.
        </div> <div class="bpc">
        Have access to <span class="bold">expert brain power</span>.
        </div> <div class="bpc">
        Are <span class="bold">a core asset</span> in content creation.
        </div>
        <div class="bpc bpc2">
        Are the platform for<br> recognizing and <span class="bold">leading future challenges</span> and trends.
        </div>
        <div class="clear"></div>
      </div>  
      <br>
      <p class="size24">
      <span class="bold">Member Benefits:</span>
      </p>
      <br>
       <div class="workgroup-container">
         <div class="join-cell-50-2">
              

           
         </div>
         <div class="join-cell-50">
          <h3>Member Only Events</h3>
          <p>
          We meet as peers in a friendly atmosphere with a mutual interest in innovating and developing financial services. Our shared passion leads to a fruitful exchange of ideas, and everyone ends up with broader insight.
          </p>
          <p>
          2 days of conference and 1 day for workgroup meetings hosted by one of the members.
          </p>
         </div>
         <div class="join-cell-50-3">
          <h3>Workgroups</h3>
          <p>
          In Mobey Forum’s workgroups you get to dig deeper into specific topics. Workgroups meet online every two weeks and they also have face-to-face sessions at every member meeting.
          </p>
          <p>
          Workgroup chairs and facilitators are recognized leaders in their fields.
          </p>
         </div>
         <div class="join-cell-50-4">
                       

         </div>
       <div class="clear"></div>
       <div class="benefits-bottom-box">
        <div class="benefits-digital-database">
        <h3>Free access to Mobey Day Events</h3>
        <p>
        </p>
        </div>
    
       
    
        <div class="benefits-presentation-archives">
         <h3>Access to Mobey Forum knowledge base</h3>
        <p>
        </p>
        
        </div>
        
    </div>       
         <div class="clear"></div> 
       
  </div>          
</section>

<section class="membership-types">
  <div class="wrapper"><br><br>
  <h2 class="center">Membership types</h2><br>
  <div style="overflow-x:auto;">
     <table class="join-table">
  <tr class="no-top-line">
    <td class="text-middle"><h4>Membership Benefits</h4></th>
    <td><h4>Associate Member</h4>
      <p class="italic size14">For smaller and innovative players</p>
      <p class="italic size13">Eligibility requirements:<br>
      Employees < 50<br>
      Turnover < 10 m €<br>
      Balance sheet total < 10 m €<br>
    </td>
     <td><h4>Senior Associate Member</h4>
      <p class="italic size14 longp">For any company</p>
    </td>
    <td><h4>Full Member</h4>
      <p class="italic size14 longp">For supervised financial institutions, such as banks or companies with financial institution specific license</p>
    </td>
    <td><h4>Advisory Member</h4>
      <p class="italic size14 longp">For any company wishing to maximize visibility within Mobey</p>
    </td>
  </tr>

  <tr>
  <td>Member meetings participation</td>
    <td class="checkmark"></td>
   <td class="checkmark"></td>
    <td class="checkmark"></td>
    <td class="checkmark"></td>

    
  </tr>
   <tr>
  <td>Workgroup participation</td>
    <td class="checkmark"></td>
   <td class="checkmark"></td>
    <td class="checkmark"></td>
    <td class="checkmark"></td>

    
  </tr>
  <tr>
  <td>Chairing workgroups</td>
  <td></td>
    <td class="checkmark"></td>
    <td class="checkmark"></td>
    <td class="checkmark"></td>
    


    
  </tr>
  <tr>
  <td>Eligibility for a seat within the Board of Directors</td>
     
     <td></td>
     <td></td>
     <td class="checkmark"></td>
     <td class="checkmark"></td>
  </tr>
      <tr>
  <td>EVoting right at general meetings</td>
     
     <td></td>
     <td></td>
     <td class="checkmark"></td>
     <td class="checkmark"></td>
  </tr>
    <tr>
  <td>Extra visibility at Mobey’s events</td>
     <td></td>
     <td></td>
     <td></td>
     <td class="checkmark"></td>
  </tr>
 
  <tr>
    <td>Yearly Price</td>
    <td class="green">5 000 € yearly</td>
    <td class="green">8 000 € yearly</td>
    <td class="green">10 000 € yearly</td>
    <td class="green">20 000 € yearly</td>
  </tr>
  
  
</table> 
   </div> 

  </div>
</section>

<section class="workgroups-access">
  <div class="wrapper">
<p class="size20">
It’s as easy as that. </p>

  <div class="clear"><a class="button primary inline" target="_blank" href="https://www.mobeyforum.org/wp-content/uploads/2017/09/Mobey-Forum-Membership-Application-Form.docx">Application Form</a></div>
  
  </div>
</section>
<section class="join-register">
  <div class="wrapper center">
    <h3>My company is a member already. I want to create an account.</h3>
    <p>If your company has completed the joining process, you can register an account below.</p>
    <div class="clear"><div class="button primary small-button thin-button register">Register</div>

  </div>          
</section>
  
<?php get_footer(); ?>

