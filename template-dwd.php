<?php  /* Template Name: Digital Wallet Database Template */ 
//if (!is_user_logged_in() ) wp_redirect( site_url( 'home' ) );
get_header(); 
 $current_user = wp_get_current_user();
 $categories = get_the_category();
?>
<!-- section -->

<section class="basic-header white-text">
   <div class="wrapper">
      <h1 class="center">Digital Wallet Database</h1>

       <div class="clear"></div>
   </div>
</section>

<!-- section -->
<section class="basicpage-main-content single-main">
   <div class="wrapper">
 
 
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

	
			</article>
			<!-- /article -->

		<?php endwhile; ?>
      <?php endif; ?>
      
      <br><br><ul>
      <?php
      $i=0;
         $args = array( 'posts_per_page' => 100,  'category' => 168  );
         
         $myposts = get_posts( $args );
         foreach ( $myposts as $post ) : setup_postdata( $post ); 
         print '<li><a class="read-more" href="'.get_the_permalink().'">'.get_the_title().'</a></li>';

   
    endforeach; 
      wp_reset_postdata();?>
   </ul>   
  </div> 
</section>   
<?php get_footer(); ?>

