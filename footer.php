			<!-- footer -->
			<footer class="footer" role="contentinfo">
        <div class="wrapper">
				
						<div class="footer-logo">
						<a href="<?php echo home_url(); ?>">
							<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
							<img src="<?php echo get_template_directory_uri(); ?>/img/mobey-forum-logo-new.png" alt="Mobey Forum" class="logo-img">
						</a>
					</div>
				
				<div class="footer-copyright">
				<span class="footer-bright">Questions? Contact us at</span> <a href="mailto:&#109;&#111;&#098;&#101;&#121;&#102;&#111;&#114;&#117;&#109;&#064;&#109;&#111;&#098;&#101;&#121;&#102;&#111;&#114;&#117;&#109;&#046;&#111;&#114;&#103;">&#109;&#111;&#098;&#101;&#121;&#102;&#111;&#114;&#117;&#109;&#064;&#109;&#111;&#098;&#101;&#121;&#102;&#111;&#114;&#117;&#109;&#046;&#111;&#114;&#103;</a><br>
					&copy; Mobey Forum  <?php echo date('Y'); ?>, all rights reserved
				</div>
				<div class="footer-social-media">
					 						<a href="https://www.facebook.com/MobeyForum"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="facebook" class="facebook-logo"></a>
					 						<a href="http://twitter.com/MobeyForum"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png" alt="facebook" class="twitter-logo"></a>
					 						<a href="http://www.linkedin.com/groups/Mobey-Forum-Mobile-payments-mobile-3441551"><img src="<?php echo get_template_directory_uri(); ?>/img/linkedin.png" alt="facebook" class="linkedin-logo"></a>

				</div>
					
			
				<!-- /copyright -->
        </div>
          <!-- /wrapper -->
			</footer>
			<!-- /footer -->

<!-- Modal -->
<div id="loginModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title">Login</h2>
      </div>
      <div class="modal-body">
        <?php  echo do_shortcode("[ultimatemember form_id=9151]"); ?>
                <p> Don’t have an account to Member Corner? <a data-dismiss="modal" class="register" href="#">Register an account here.</a></p>
      </div>
     </div>

  </div>
</div>

<!-- Modal -->
<div id="registerModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title">Register an account</h2>
      </div>
      <div class="modal-body">
          <p>Here you can register an employee account.</p>
          <p>Looking to join us as a <b>company?</b><br>
          <a href="/join/">Click here to view the details.</a></p>

          <div class="modal-warning-message">
          <div class="warning-sign-right"></div>
          <div class="warning-sign-text">
          Only employees of <b>Mobey Forum Member Companies</b> can register an account.
          </div>
          <div class="warning-sign-left"></div>
          </div>      
        <h3>Registration form</h3>
        <?php  echo do_shortcode("[ultimatemember form_id=9150]"); ?>
      </div>
     </div>

  </div>
</div>


		

		<?php wp_footer(); ?>

		<!-- analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-45202764-1', 'auto');
  ga('send', 'pageview');
</script>

	</body>
</html>
