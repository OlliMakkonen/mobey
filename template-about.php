<?php  /* Template Name: About Page Template */ 

//if (!is_user_logged_in() ) wp_redirect( site_url( 'home' ) );
get_header(); 
 $current_user = wp_get_current_user();
?>
<!-- section -->

<section class="basic-header white-text">
   <div class="wrapper">
      <h1 class="center">About</h1>

       <div class="clear"></div>
   </div>
</section>

<!-- section -->
<section class="basicpage-main-content about-main">
   <div class="wrapper">
  
      <?php if (have_posts()): while (have_posts()) : the_post(); ?>
      <!-- article -->
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
         <?php the_content(); ?>
         <br class="clear">
      </article>
      <!-- /article -->
      <?php endwhile; ?>
      <?php endif; ?>
      
      
   

      <div class="about-for-container">
        <div class="about-red-line1">    
        </div>
        <div class="about-for-our">  
        For our   
        
        <img style="vertical-align:middle" src="<?php echo get_template_directory_uri(); ?>/img/72-counting.png" alt="72 and counting" class="">

        
        member companies..  
        </div>
        <div class="about-red-line2">    
        </div>
      </div>                 


         
<div class="clear"></div>
    </div>
          
</section>
<section class="about-we-organize">
  <div class="wrapper">
     <p class="size30 bold center">We Organize</p>
     <div class="organize-box">
        <div class="organizeimg1">
          </div>
       <div class="organize-ab1">
       <h3>Quarterly Events</h3>
       <p>All around the world with both open for all and exclusive members only meetings</p>
       
       </div>
        <div class="organize-and-icon"></div>
       
          <div class="organizeimg2">
          </div> 
        <div class="organize-ab2">
       <h3>Workgroup Meetings</h3>
       <p>Where members can discuss and analyse financial industry developments onlines</p>
     </div> 
     
     <div class="clear"></div>
   </div>
   <div class="clear"></div>
       <div class="workgroups-meet">Our four workgroups meet, discuss and ultimately produce reports that are published in our Publications section</div>

  </div>          
</section>
<section class="about-publications">
  <div class="wrapper">
  <div class="line-mask"></div>
    <h3 class="center">Publications</h3>
    <div class="about-pboxes">
      <div class="about-pbox about-pbox1"><a href="/category/blog/">Blogs</a></div>
      <div class="about-pbox about-pbox2"><a href="/category/press-releases/">Press releases</a></div>
      <div class="about-pbox about-pbox3"><a href="/category/whitepapers/">Reports</a></div>
      <div class="about-pbox about-pbox4"><a href="/category/videos/">Videos</a></div>
      <div class="about-pbox about-pbox5"><a href="/category/webinars/">Webinars</a></div>
    </div> 
    <div class="clear"></div>
     <div class="open-publications">Publications are open to everybody free of charge</div>
    
    <div class="about-picons">
   <img src="<?php echo get_template_directory_uri(); ?>/img/digital-wallet-database.png" alt="Digital Wallet database" class="s50 left">

   <img src="<?php echo get_template_directory_uri(); ?>/img/presentation-archive.png" alt="Presentation Archive" class="s50 right">
    
    </div>
    <div class="clear"></div>
    <div class="size24">
    we also offer a <b>digital wallet database</b> and a <b>presentation archive</b> as member benefits!
    </div>

  
  </div>          
</section>


 <section class="join-mobey-forum">
   <div class="wrapper">
      <h3 class="white">Find out more about member benefits</h3>
      <p class="size20">Check out how to join!</p>
      <p class="hero-buttons"><a class="button primary inline" href="/join/">Join</a></p>
   </div>
</section>  


<section class="about-board">
  <div class="wrapper center">
  <h2 class="">Board of Directors</h2>
  
          <?php
        wp_reset_postdata();  

    $query = new WP_Query( array(
    'post_type' => 'boardmembers',          // name of post type.
     'orderby' => 'menu_order', 
    'order' => 'ASC', 
    'tax_query' => array(
        array(
            'taxonomy' => 'types',   // taxonomy name
            'field' => 'slug',           // term_id, slug or name
            'terms' => 'boardmember', 
                             // term id, term slug or term name
        )
    )
) );

while ( $query->have_posts() ) : $query->the_post();
$post_id = get_the_ID();
$custom = get_post_custom($post_id);
               $bank= $custom["bank"][0]; 

                print ' <div class="boardbox" style="background: url('.get_the_post_thumbnail_url().') no-repeat center center; background-size:  281px 375px;"><div class="bb-button"></div><div class="bb-content">';
 
                print '<span class="name">'.get_the_title().'</span><br>';
                print '<span class="bank">'.$bank.'</span><br>';
                print '<div class="bb-extra-info">';
                
                print get_the_content();
                print "</div></div></div>";

endwhile;

      wp_reset_postdata();?>
  

  <div class="clear"></div>
   <div class="clear"><br><br></div>
  <h2 class="">Mobey Team</h2>
  
  
    
    <?php
function hexentities($str) {
    $return = '';
    for($i = 0; $i < strlen($str); $i++) {
        $return .= '&#x'.bin2hex(substr($str, $i, 1)).';';
    }
    return $return;
}    
    
    $query = new WP_Query( array(
    'post_type' => 'boardmembers',          // name of post type.
         'orderby' => 'menu_order', 
    'order' => 'ASC', 
    'tax_query' => array(
        array(
            'taxonomy' => 'types',   // taxonomy name
            'field' => 'slug',           // term_id, slug or name
            'terms' => 'team',                  // term id, term slug or term name
        )
    )
) );

while ( $query->have_posts() ) : $query->the_post();
$post_id = get_the_ID();
$custom = get_post_custom($post_id);
               $bank= $custom["bank"][0]; 
               $email= $custom["email"][0]; 

                print ' <div class="boardbox" style="background: url('.get_the_post_thumbnail_url().') no-repeat center center; background-size:  281px 375px;"><div class="bb-button"></div><div class="bb-content">';
 
                print '<span class="name">'.get_the_title().'</span><br>';
                print '<span class="bank">'.$bank.'</span><br>';
                print '<a class="bb-email" href="mailto:'.hexentities($email).'">'.hexentities($email).'</a>';
                print '<div class="bb-extra-info">';
                
                print get_the_content();
                print "</div></div></div>";

endwhile;

      wp_reset_postdata();?>
    
    
      
 
     
  <div class="clear"></div>
  
  
  </div>
</section>
<section class="about-corporate-members">
  <div class="wrapper center">
      <h2>Mobey Members</h2>
      <div id="CorporateCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#CorporateCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#CorporateCarousel" data-slide-to="1"></li>
        <li data-target="#CorporateCarousel" data-slide-to="2"></li>
        <li data-target="#CorporateCarousel" data-slide-to="3"></li>
      </ol>
        <div class="carousel-inner">
          <div class="item active">

<?php
$path  = "wp-content/themes/mobey-theme2/img/members/";
$allowed_extensions = Array('jpg','png');
 $i=0;
if ($handle = opendir($path )) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            
            $file_parts = pathinfo($path.$entry);

            $file_parts['extension'];
           

if (in_array($file_parts['extension'], $allowed_extensions)){
    print '<div class="cm-box"><div><img src="'.get_template_directory_uri().'/img/members/'.$entry.'" alt="member logo" class="member-logo"></div></div>';
$i++;
}
     if ($i==18){$i=0; print '</div><div class="item">';}
            
            
        }
    }
    closedir($handle);
}
?>
        </div>
      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#CorporateCarousel" data-slide="prev">
        
      </a>
      <a class="right carousel-control" href="#CorporateCarousel" data-slide="next">
        
      </a>
    </div>
  
  
  </div>
</section>
<section class="about-member-news">
  <div class="wrapper">
     <h3 class="center">News from our Members</h2>
     
      <div class="member-news">
                 <?php
           $i=0;
            $args = array( 'posts_per_page' => 6, 'category' => 8 );
            
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post ); 
            print "<div class=\"div$i news-container\">";
            $i++;
            ?>
         <div class="heading-div"><a class="news-heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
         <div class="news-date"><?php print get_the_date('jS \o\f F Y'); ?></div>
         <div class="news-link"><a class="read-more" href="<?php the_permalink(); ?>">Read more</a></div>
      </div>
      <?php endforeach; 
         wp_reset_postdata();?>
   </div> 
   <div class="clear"></div>
   </div>
</section>   
<?php get_footer(); ?>

