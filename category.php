<?php get_header(); ?>
<!-- section -->

<section class="basic-header white-text">
   <div class="wrapper center"><br>
 <h1 class="center">Publications</h1>
 <form class="search " method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-input1" type="search" name="s" placeholder="<?php _e( 'Search Publications', 'html5blank' ); ?>">
	<input type="hidden" name="post_types" value="post" />
	<button class="search-submit button primary" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
</form>
   </div>
</section>
<?php 
        $category = get_the_category(); 
?>        
<section class="publications-menu <?php print $category[0]->slug; ?>-pb">
    <div class="wrapper">
      <ul class="p-menu">
          <li><a class="pbb" href="/category/blog/"><span>Blogs</span></a></li>
          <li><a class="pbp" href="/category/press-releases/"><span>Press releases</span></a></li>
          <li><a class="pbr" href="/category/whitepapers/"><span>Reports</span></a></li>
          <li><a class="pbv" href="/category/videos/"><span>Videos</span></a></li>
          <li><a class="pbw" href="/category/webinars/"><span>Webinars</span></a></li>
      </ul>
   </div>
</section>

<section class="publications-reports-blogs">
   <div class="wrapper">
   
 
  
        
        <?php 
       
        echo do_shortcode('[ajax_filter_posts_mt cat="'.$category[0]->cat_ID.'" per_page="10"]'); ?>
        
 <div class="clear"></div>
   </div>
  
   
       
</section>
<?php get_footer(); ?>

