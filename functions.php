<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 600, 436, true); // Large Thumbnail
    add_image_size('medium', 285, 214, true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('dashboard', 261, 280, true); // Medium Thumbnail   
    add_image_size('reports', 9999, 148, false); // reports Thumbnail

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s<li><li class="login-button"><span class="login-icon">Login</span></li><li class="logout-button"><a href="/logout">logout</a></li><li><div class="search-icon"></div></li></ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.6'); // Custom scripts
        wp_enqueue_script('html5blankscripts'); // Enqueue it!
        
        wp_register_script('topnav', get_template_directory_uri() . '/js/topnav.js', array('jquery'), '1.0.1'); // Custom scripts
        wp_enqueue_script('topnav'); // Enqueue it!
     
        wp_register_script('bootstrapjs', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('bootstrapjs'); // Enqueue it!
        
          wp_register_script('ajax-filter-posts', get_template_directory_uri() . '/js/ajax-filter-posts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('ajax-filter-posts'); // Enqueue it!
        
               wp_register_script('atc-min', get_template_directory_uri() . '/js/atc.min.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('atc-min'); // Enqueue it!
        
        //wp_register_script('responsive-tables', get_template_directory_uri() . '/js/responsive-tables.js', array('jquery'), '1.0.0'); // Custom scripts
         //wp_enqueue_script('responsive-tables'); // Enqueue it!
        
        wp_localize_script( 'ajax-filter-posts', 'bobz', array(
        'nonce'    => wp_create_nonce( '1234codens' ),
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));
                
    }
}

// Load HTML5 Blank conditional scripts
function my_enqueue_admin_scripts()
{
   
        wp_enqueue_script( 'pickaday', get_template_directory_uri() . '/js/pickaday.js', array ( 'jquery' ), 1.1, true);
        wp_enqueue_style( 'pickadaycss', get_template_directory_uri() . '/js/pikaday.css',false,'1.1','all');

   
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    wp_register_style('bootstrap', get_template_directory_uri() . '/bootstrap.min.css', array(), '1.0', 'all');
    wp_enqueue_style('bootstrap'); // Enqueue it!
    
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!
   
   
   wp_register_style('atc-style-blue', get_template_directory_uri() . '/atc-style-blue.css', array(), '1.4', 'all');
    wp_enqueue_style('atc-style-blue'); // Enqueue it!
   
     //  wp_register_style('responsive-tables', get_template_directory_uri() . '/responsive-tables.css', array(), '1.4', 'all');
     //wp_enqueue_style('responsive-tables'); // Enqueue it!
   
    wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '2.5', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!
    
    
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Main Menu', 'html5blank') // Main Navigation
      ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 10;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 8;
}
// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post_long($length)
{
    return 13;
}


// Create the Custom Excerpts callback
function html5wp_excerpt($length = 20, $more_callback = '')
{
    global $post;
    
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
     $output = wp_trim_words($output,$length);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action("admin_init", "admin_init");
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head

add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_member_logos'); // webinar custom post type
add_action('init', 'create_post_type_board_members'); // webinar custom post type
add_action('init', 'create_post_type_videos'); // videos custom post type
add_action('init', 'create_post_type_events'); // videos custom post type

add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination
  add_action('save_post', 'save_meta_event_options');
add_action('admin_enqueue_scripts', 'my_enqueue_admin_scripts');
 add_action('save_post', 'save_meta_webinars_options');
 add_action('save_post', 'save_meta_videos_options');
  add_action('save_post', 'save_meta_boardmembers_options');
    add_action('save_post', 'save_meta_logos_options');
// Remove Actions


add_shortcode('dt_vc_list', 'dt_vc_list_function');
function dt_vc_list_function($atts, $content = null) {
return $content;
}

add_shortcode('dt_fancy_separator', 'dt_fancy_separator_function');
function dt_fancy_separator_function($atts, $content = null) {
return '<div class="separator"><div class="red-line"></div></div>';
}


add_shortcode('dt_button', 'button_function');
function button_function($atts, $content = null) {
extract(shortcode_atts(array(
      'link' => "",
      'target_blank' => "false",
   ), $atts));
   
return '<a class="button primary" href="'.$link.'" target="_blank" />'.$content.'</a>';

}

add_shortcode('mobey_logos', 'mobey_logos_function');
function mobey_logos_function($atts, $content = null) {
ob_start();
?>

   <div id="CorporateCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#CorporateCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#CorporateCarousel" data-slide-to="1"></li>
        <li data-target="#CorporateCarousel" data-slide-to="2"></li>
        <li data-target="#CorporateCarousel" data-slide-to="3"></li>
      </ol>
        <div class="carousel-inner">
          <div class="item active">

<?php
$path  = "wp-content/themes/mobey-theme2/img/members/";
$allowed_extensions = Array('jpg','png');
 $i=0;
if ($handle = opendir($path )) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            
            $file_parts = pathinfo($path.$entry);

            $file_parts['extension'];
           

if (in_array($file_parts['extension'], $allowed_extensions)){
    print '<div class="cm-box"><div><img src="'.get_template_directory_uri().'/img/members/'.$entry.'" alt="member logo" class="member-logo"></div></div>';
$i++;
}
     if ($i==18){$i=0; print '</div><div class="item">';}
            
            
        }
    }
    closedir($handle);
}
?>
        </div>
      </div>
      <!-- Left and right controls -->
      <a class="left carousel-control" href="#CorporateCarousel" data-slide="prev">        
      </a>
      <a class="right carousel-control" href="#CorporateCarousel" data-slide="next">
        
      </a>
    </div>
<?php
$result = ob_get_clean();
return $result;
}

add_shortcode('dt_blog_scroller', 'dt_blog_scroller_function');
function dt_blog_scroller_function($atts, $content = null) {
extract(shortcode_atts(array(
      'autoslide' => "3000",
      'loop' => "true",
      'category' => ''
   ), $atts));

	$args = [
		'post_type'      => 'post',
		'post_status'    => 'publish',
		'category_name' => $category,
	];

	$qry = new WP_Query($args);

	$i=0;
	ob_start();
	?>
      
	<?php
		if ($qry->have_posts()) :
			while ($qry->have_posts()) : $qry->the_post(); 
			if ( has_post_thumbnail() ) {
	

print '<p class="center"><a href="'.get_the_permalink().'">'; print "\n";
the_post_thumbnail( 'full' ); 
print '</a></p>';

$i++;
}
endwhile;
endif;


?>

<?php

   
 $result = ob_get_clean();
return $result;
}


   function admin_init(){
add_meta_box("Event-meta", "Event info", "meta_event_options", "Events", "normal", "low");
add_meta_box("Webinars-meta", "Webinar info", "meta_webinars_options", "Webinars", "normal", "low");
add_meta_box("videos-meta", "Information", "meta_videos_options", "Videos", "normal", "low");
add_meta_box("boardmembers-meta", "Information", "meta_boardmembers_options", "Boardmembers", "normal", "low");
add_meta_box("logos-meta", "Information", "meta_logos_options", "Logos", "normal", "low");
    }

remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images
add_filter( 'template_include', 'mobey_event_template');

function mobey_event_template( $template )
{
    if ( 'events' === get_post_type() )
        return dirname( __FILE__ ) . '/mobey_event_template.php';

    return $template;
}

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

function save_meta_boardmembers_options(){
    global $post;
    update_post_meta($post->ID, "bank", $_POST["bank"]);
    update_post_meta($post->ID, "email", $_POST["email"]);


}

    function meta_boardmembers_options(){
        global $post;
        $custom = get_post_custom($post->ID);
        $custom_field1 = $custom["bank"][0];
        $custom_field2 = $custom["email"][0];
 
?>
<label>Bank:</label><br><input type="text" size="80" class="" name="bank" value="<?php echo $custom_field1; ?>" /><br>
<label>Email:</label><br><input type="text" size="80" class="" name="email" value="<?php echo $custom_field2; ?>" /><br>

<?php
    }



function save_meta_webinars_options(){
    global $post;
    update_post_meta($post->ID, "alkaa_pv", $_POST["alkaa_pv"]);
    update_post_meta($post->ID, "alkaa_klo", $_POST["alkaa_klo"]);
    update_post_meta($post->ID, "loppuu_pv", $_POST["loppuu_pv"]);
    update_post_meta($post->ID, "loppuu_klo", $_POST["loppuu_klo"]);
        update_post_meta($post->ID, "paikka", $_POST["paikka"]);
           update_post_meta($post->ID, "registration_link", $_POST["registration_link"]);

}

    function meta_webinars_options(){
        global $post;
        $custom = get_post_custom($post->ID);
        $custom_field1 = $custom["alkaa_pv"][0];
         $custom_field2= $custom["alkaa_klo"][0];
          $custom_field3 = $custom["loppuu_klo"][0];
         $custom_field4 = $custom["paikka"][0];
          $custom_field5 = $custom["registration_link"][0];

 
  
?>
    <label>Date:</label><br><input type="text" id="datepicker" class="datepicker" name="alkaa_pv" value="<?php echo $custom_field1; ?>" /><br>
  <label>Starts time EDT: (i.e. 1pm) </label><br><input type="text" class="" name="alkaa_klo" value="<?php echo $custom_field2; ?>" /><br>

    <label>Ends time:</label><br><input type="text" class="" name="loppuu_klo" value="<?php echo $custom_field3; ?>" /><br>

<label>Registration link:</label><br><input type="text" size="80" class="" name="registration_link" value="<?php echo $custom_field5; ?>" /><br>

<label>YouTube-link:</label><br><input type="text" size="80" class="" name="paikka" value="<?php echo $custom_field4; ?>" /><br>
<?php
    }
 

function create_post_type_events()
{
    register_taxonomy_for_object_type('category', 'Events'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'Events');
    register_post_type('Events', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Events', 'Events'), // Rename these to suit
            'singular_name' => __('Event', 'Events'),
            'add_new' => __('Add New', 'Events'),
            'add_new_item' => __('Add New Event', 'Events'),
            'edit' => __('Edit', 'Events'),
            'edit_item' => __('Edit Event', 'Events'),
            'new_item' => __('New Event', 'Events'),
            'view' => __('View Event', 'Events'),
            'view_item' => __('View Event', 'Events'),
            'search_items' => __('Search Event', 'Events'),
            'not_found' => __('No Events found', 'Events'),
            'not_found_in_trash' => __('No Events found in Trash', 'Events')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => false,
         'menu_icon'   => 'dashicons-calendar-alt',
       'supports' => array(
            'title',
             'editor',
            'category',
            'thumbnail',
            'page-attributes'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
 'taxonomies' => array(
            'category'
        ) // Add Category and Post Tags support
    ));
}

function create_post_type_member_logos()
{
    register_taxonomy_for_object_type('category', 'Logos'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'Logos');
    register_post_type('Logos', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Logos', 'Logos'), // Rename these to suit
            'singular_name' => __('Logo', 'Logos'),
            'add_new' => __('Add New', 'Logos'),
            'add_new_item' => __('Add New Logo', 'Logos'),
            'edit' => __('Edit', 'Logos'),
            'edit_item' => __('Edit Logo', 'Logos'),
            'new_item' => __('New Logo', 'Logos'),
            'view' => __('View Logo', 'Logos'),
            'view_item' => __('View Logo', 'Logos'),
            'search_items' => __('Search Logo', 'Logos'),
            'not_found' => __('No Logos found', 'Logos'),
            'not_found_in_trash' => __('No Logos found in Trash', 'Logos')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => false,
        'supports' => array(
            'title',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true // Allows export in Tools > Export

    ));
}


add_action( 'init', 'create_boardmember_category' );

function create_boardmember_category() {
	register_taxonomy(
		'types',
		'boardmembers',
		array(
			'label' => __( 'Type' ),
			'rewrite' => array( 'slug' => 'type' ),
			'hierarchical' => true,
		)
	);
}


function create_post_type_board_members()
{
register_post_type('boardmembers', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Boardmembers', 'boardmembers'), // Rename these to suit
            'singular_name' => __('Boardmember', 'boardmembers'),
            'add_new' => __('Add New', 'boardmembers'),
            'add_new_item' => __('Add New Boardmember', 'boardmembers'),
            'edit' => __('Edit', 'boardmembers'),
            'edit_item' => __('Edit Boardmember', 'boardmembers'),
            'new_item' => __('New Boardmember', 'boardmembers'),
            'view' => __('View Boardmember', 'boardmembers'),
            'view_item' => __('View Boardmember', 'boardmembers'),
            'search_items' => __('Search Boardmember', 'boardmembers'),
            'not_found' => __('No Boardmembers found', 'boardmembers'),
            'not_found_in_trash' => __('No Boardmembers found in Trash', 'boardmembers')
            
        ),
        'taxonomies'  => array( 'type' ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => false,
        'exclude_from_search' => true,
        'supports' => array(
            'title',
            'editor',
            'category',
            'thumbnail',         
            'page-attributes'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true // Allows export in Tools > Export

    ));
     register_taxonomy_for_object_type('types', 'boardmembers'); // Register Taxonomies for Category
}
// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_videos()
{
    register_taxonomy_for_object_type('category', 'Videos'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'Videos');
    register_post_type('Videos', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Videos', 'Videos'), // Rename these to suit
            'singular_name' => __('Video', 'Videos'),
            'add_new' => __('Add New', 'Videos'),
            'add_new_item' => __('Add New', 'Videos'),
            'edit' => __('Edit', 'Videos'),
            'edit_item' => __('Edit Video', 'Videos'),
            'new_item' => __('New Video', 'Videos'),
            'view' => __('View Video', 'Videos'),
            'view_item' => __('View Video', 'Videos'),
            'search_items' => __('Search Video', 'Videos'),
            'not_found' => __('No Videos found', 'Videos'),
            'not_found_in_trash' => __('No Videos found in Trash', 'Videos')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => false,
        'supports' => array(
            'title',
            'category',
            'thumbnail',
            'page-attributes'
            
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
 'taxonomies' => array(
            'category'
        ) // Add Category and Post Tags support
    ));
}

 
function save_meta_videos_options(){
    global $post;
        update_post_meta($post->ID, "youtube-url", $_POST["youtube-url"]);
}  
function meta_videos_options(){
        global $post;
        $custom = get_post_custom($post->ID);
        $youtubeID = $custom["youtube-url"][0];  
?>
<label>YouTube url:</label><br><input type="text" size="80" class="" name="youtube-url" value="<?php echo $youtubeID; ?>" /><br>
<?php
}
 
function save_meta_logos_options(){
    global $post;
        update_post_meta($post->ID, "logo-url", $_POST["logo-url"]);
}  
function meta_logos_options(){
        global $post;
        $custom = get_post_custom($post->ID);
        $logo = $custom["logo-url"][0];  
?>
<label>Url:</label><br><input type="text" size="80" class="" name="logo-url" value="<?php echo $logo; ?>" /><br>
<?php
}
 



    function meta_event_options(){
        global $post;
       
        $custom = get_post_custom($post->ID);
         $location = $custom["location"][0];
         $heading = $custom["heading"][0];
         $theme = $custom["theme"][0];
         
         $pb_text = $custom["pb-text"][0];
         $pb_url = $custom["pb-url"][0];
         $sb_text = $custom["sb-text"][0];
         $sb_url = $custom["sb-url"][0];
         
         $date2= $custom["alkaa_pv"][0];
          
          $date3 = $custom["loppuu_pv"][0];
          $eventbrite_id = $custom["eventbrite_id"][0];
           $gravity_forms = $custom["gravity_forms"][0];
          $short_info_text = $custom["short_info_text"][0];
          $press_code = $custom["press_code"][0];
           $booked = $custom["booked"][0];
           $members_only = $custom["members_only"][0];
           $past_event = $custom["past_event"][0];
           
           if ($booked==1) $booked = "checked"; else $booked="";
           if ($members_only==1) $members_only = "checked"; else $members_only="";
           if ($past_event==1) $past_event = "checked"; else $past_event="";

?>
  <label>Location:</label><br><input size="80" type="text" name="location" value="<?php echo $location; ?>" /><br>
  <label>Heading:</label><br><input size="80" type="text" name="heading" value="<?php echo $heading; ?>" /><br>
  <label>Theme:</label><br><input size="80" type="text" name="theme" value="<?php echo $theme; ?>" /><br>



<label>Short info text:</label><br><textarea cols="80" rows="8" name="short_info_text"><?php echo $short_info_text; ?></textarea><br>
  <label>Primary button text:</label><br><input size="80" type="text" name="pb-text" value="<?php echo $pb_text; ?>" /><br>

  <label>Primary button url:</label><br><input size="80" type="text" name="pb-url" value="<?php echo $pb_url; ?>" /><br>

  <label>Secondary button text:</label><br><input size="80" type="text" name="sb-text" value="<?php echo $sb_text; ?>" /><br>

  <label>Secondary button url:</label><br><input size="80" type="text" name="sb-url" value="<?php echo $sb_url; ?>" /><br>



    <label>Start date:</label><br><input type="text" id="datepicker" class="datepicker" name="alkaa_pv" value="<?php echo $date2; ?>" /><br>

    <label>End date:</label><br><input type="text" id="datepicker2" class="datepicker" name="loppuu_pv" value="<?php echo $date3; ?>" /><br>
    <label>Eventbrite ID:</label><br><input type="text" id="" class="eventbrite" name="eventbrite_id" value="<?php echo $eventbrite_id; ?>" /><br>
     <label>Members register Gravity form ID:</label><br><input type="text" id="" class="gravity_forms" name="gravity_forms" value="<?php echo $gravity_forms; ?>" /><br>
    <label>Press code:</label><br><input type="text" id="" class="eventbrite" name="press_code" value="<?php echo $press_code; ?>" /><br>

<label>Fully booked: </label><input type="checkbox" value="1" class="" name="booked" <?php echo $booked; ?>/><br>
<label>Members only: </label><input type="checkbox" value="1" class="" name="members_only" <?php echo $members_only; ?>/><br>
<label>Past event: </label><input type="checkbox" value="1" class="" name="past_event" <?php echo $past_event; ?>/><br>

<?php
    }
 
function save_meta_event_options(){
    global $post;
update_post_meta($post->ID, "short_info_text", $_POST["short_info_text"]);
        update_post_meta($post->ID, "alkaa_pv", $_POST["alkaa_pv"]);
    update_post_meta($post->ID, "loppuu_pv", $_POST["loppuu_pv"]);
     update_post_meta($post->ID, "location", $_POST["location"]);
       update_post_meta($post->ID, "booked", $_POST["booked"]);
        update_post_meta($post->ID, "past_event", $_POST["past_event"]);
         update_post_meta($post->ID, "members_only", $_POST["members_only"]);
         update_post_meta($post->ID, "theme", $_POST["theme"]);
         
      update_post_meta($post->ID, "pb-text", $_POST["pb-text"]);
      update_post_meta($post->ID, "pb-url", $_POST["pb-url"]);
      update_post_meta($post->ID, "sb-text", $_POST["sb-text"]);
      update_post_meta($post->ID, "sb-url", $_POST["sb-url"]);
         
           update_post_meta($post->ID, "heading", $_POST["heading"]);
       update_post_meta($post->ID, "eventbrite_id", $_POST["eventbrite_id"]);
       update_post_meta($post->ID, "gravity_forms", $_POST["gravity_forms"]);
         update_post_meta($post->ID, "press_code", $_POST["press_code"]);
}
    


add_action( 'admin_menu', 'addAdminMenu' );

function addAdminMenu(){
	add_action( 'admin_init', 'register_mobey_settings' );
add_menu_page('Mobey Admin Settings', 'Theme settings', 'manage_options', 'mobey_admin_settings_page', 'mobey_settings_page', '', 100);
add_submenu_page('mobey_admin_settings_page','Frontpage', 'Frontpage','manage_options', 'mobey_frontpage_slider', 'mobey_frontpage_settings_page');
add_submenu_page('mobey_admin_settings_page','Past Events', 'Past Events','manage_options', 'mobey_past_events', 'mobey_past_events_settings_page');
add_submenu_page('mobey_admin_settings_page','Workgroups', 'Workgroups','manage_options', 'mobey_workgroups', 'mobey_workgroups_settings_page');

}
function register_mobey_settings() {
	//register our settings
	register_setting( 'mobey-slide_settings-group', 'slide_settings' );
	register_setting( 'mobey-past-events-group', 'mobey_past_events' );
	register_setting( 'mobey-workgroups-group', 'mobey_workgroups' );
	}



function mobey_frontpage_settings_page() {
?>
<div class="wrap">
<h1>Frontpage </h1>

<form method="post" action="options.php">
    <?php settings_fields( 'mobey-slide_settings-group' ); ?>
    <?php do_settings_sections( 'mobey-slide_settings-group' ); ?>
    <?php $slide_settings =  get_option('slide_settings');
    
    
    
   ?>

    <table class="form-table">
    <tr valign="top">
        <td colspan="2"><h2>Slide 1</h2></td>
        </tr>
        <tr valign="top">
        <th scope="row">Slide 1 Main heading </th>
        <td><input type="text" size="160" name="slide_settings[hero_heading]" value="<?php echo $slide_settings['hero_heading']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Main text/image url </th>
        <td><textarea cols="80" rows="8" name="slide_settings[hero_text]"><?php echo $slide_settings['hero_text']; ?></textarea></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Button 1 link text</th>
        <td><input type="text" size="160" name="slide_settings[button_1_text]" value="<?php echo $slide_settings['button_1_text']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Button 1 link url</th>
        <td><input type="text" size="160" name="slide_settings[button_1_link]" value="<?php echo $slide_settings['button_1_link']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Button 2 link text</th>
        <td><input type="text" size="160" name="slide_settings[button_2_text]" value="<?php echo $slide_settings['button_2_text']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Button 2 link url</th>
        <td><input type="text" size="160" name="slide_settings[button_2_link]" value="<?php echo $slide_settings['button_2_link']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Slide indicator Button</th>
        <td><input type="text" size="160" name="slide_settings[slide_button1]" value="<?php echo $slide_settings['slide_button1']; ?>" /></td>
        </tr>
        <tr valign="top">
        <td colspan="2"><hr><h2>Slide 2</h2></td>
        </tr>
        
               <tr valign="top">
        <th scope="row">Slide 2 Main heading </th>
        <td><input type="text" size="160" name="slide_settings[hero_heading2]" value="<?php echo $slide_settings['hero_heading2']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Main text/image url</th>
        <td><textarea cols="80" rows="8" name="slide_settings[hero_text2]"><?php echo $slide_settings['hero_text2']; ?></textarea></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Button 1 link text</th>
        <td><input type="text" size="160" name="slide_settings[button_1_text2]" value="<?php echo $slide_settings['button_1_text2']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Button 1 link url</th>
        <td><input type="text" size="160" name="slide_settings[button_1_link2]" value="<?php echo $slide_settings['button_1_link2']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Button 2 link text</th>
        <td><input type="text" size="160" name="slide_settings[button_2_text2]" value="<?php echo $slide_settings['button_2_text2']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Button 2 link url</th>
        <td><input type="text" size="160" name="slide_settings[button_2_link2]" value="<?php echo $slide_settings['button_2_link2']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Slide 2 indicator Button</th>
        <td><input type="text" size="160" name="slide_settings[slide_button2]" value="<?php echo $slide_settings['slide_button2']; ?>" /></td>
        </tr>
        
                <tr valign="top">
        <td colspan="2"><hr><h2>Slide 3</h2></td>
        </tr>
        
               <tr valign="top">
        <th scope="row">Slide 3 Main heading </th>
        <td><input type="text" size="160" name="slide_settings[hero_heading3]" value="<?php echo $slide_settings['hero_heading3']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Main text/image url </th>
        <td><textarea cols="80" rows="8" name="slide_settings[hero_text3]"><?php echo $slide_settings['hero_text3']; ?></textarea></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Button 1 link text</th>
        <td><input type="text" size="160" name="slide_settings[button_1_text3]" value="<?php echo $slide_settings['button_1_text3']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Button 1 link url</th>
        <td><input type="text" size="160" name="slide_settings[button_1_link3]" value="<?php echo $slide_settings['button_1_link3']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Button 2 link text</th>
        <td><input type="text" size="160" name="slide_settings[button_2_text3]" value="<?php echo $slide_settings['button_2_text3']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Button 2 link url</th>
        <td><input type="text" size="160" name="slide_settings[button_2_link3]" value="<?php echo $slide_settings['button_2_link3']; ?>" /></td>
        </tr>
        <tr valign="top">
        <th scope="row">Slide 3 indicator Button</th>
        <td><input type="text" size="160" name="slide_settings[slide_button3]" value="<?php echo $slide_settings['slide_button3']; ?>" /></td>
        </tr>

                  <tr valign="top">
        <td colspan="2"><hr><h2>Slide 4</h2></td>
        </tr>
        
               <tr valign="top">
        <th scope="row">Slide 4 Main heading </th>
        <td><input type="text" size="160" name="slide_settings[hero_heading4]" value="<?php echo $slide_settings['hero_heading4']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Main text/image url </th>
        <td><textarea cols="80" rows="8" name="slide_settings[hero_text4]"><?php echo $slide_settings['hero_text4']; ?></textarea></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Button 1 link text</th>
        <td><input type="text" size="160" name="slide_settings[button_1_text4]" value="<?php echo $slide_settings['button_1_text4']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Button 1 link url</th>
        <td><input type="text" size="160" name="slide_settings[button_1_link4]" value="<?php echo $slide_settings['button_1_link4']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Button 2 link text</th>
        <td><input type="text" size="160" name="slide_settings[button_2_text4]" value="<?php echo $slide_settings['button_2_text4']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Button 2 link url</th>
        <td><input type="text" size="160" name="slide_settings[button_2_link4]" value="<?php echo $slide_settings['button_2_link4']; ?>" /></td>
        </tr>
        <tr valign="top">
        <th scope="row">Slide 4 indicator Button</th>
        <td><input type="text" size="160" name="slide_settings[slide_button4]" value="<?php echo $slide_settings['slide_button4']; ?>" /></td>
        </tr>
        
    
    </table>
     <h2>Workgroups</h2>  
      <table class="form-table">
     <tr valign="top">
        <th scope="row">Workgroups upper text</th>
        <td>
         <?php
         
                         $settings = array(
    'teeny' => false,
    'textarea_rows' => 12,
    'tabindex' => 1,
    'textarea_name' => 'slide_settings[workgroups_upper_text]'
);
wp_editor($slide_settings['workgroups_upper_text'], 'workgroups_upper_text', $settings);
         ?>
         </td></tr>
         
            <tr valign="top">
        <th scope="row">Workgroups lower text</th>
        <td>
         <?php
         
                         $settings = array(
    'teeny' => false,
    'textarea_rows' => 12,
    'tabindex' => 1,
    'textarea_name' => 'slide_settings[workgroups_lower_text]'
);
wp_editor($slide_settings['workgroups_lower_text'], 'workgroups_lower_text', $settings);
         ?>
         </td></tr>  
         
              <tr valign="top">
        <th scope="row">Theme 1 text</th>
        <td><input type="text" size="160" name="slide_settings[theme_1_text]" value="<?php echo $slide_settings['theme_1_text']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Theme 2 text</th>
        <td><input type="text" size="160" name="slide_settings[theme_2_text]" value="<?php echo $slide_settings['theme_2_text']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Theme 3 text</th>
        <td><input type="text" size="160" name="slide_settings[theme_3_text]" value="<?php echo $slide_settings['theme_3_text']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Theme 4 text</th>
        <td><input type="text" size="160" name="slide_settings[theme_4_text]" value="<?php echo $slide_settings['theme_4_text']; ?>" /></td>
        </tr>
            <tr valign="top">
        <th scope="row">Button link text</th>
        <td><input type="text" size="160" name="slide_settings[themebutton_text]" value="<?php echo $slide_settings['themebutton_text']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Button link url</th>
        <td><input type="text" size="160" name="slide_settings[themebutton_link]" value="<?php echo $slide_settings['themebutton_link']; ?>" /></td>
        </tr>
        
         
     </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php }         
        
function mobey_past_events_settings_page() {
?>
<div class="wrap">
<h1>Past Events Keynotes & Highlights </h1>

<form method="post" action="options.php">
    <?php settings_fields( 'mobey-past-events-group' ); ?>
    <?php do_settings_sections( 'mobey_past_events-group' ); ?>
    <?php $mobey_past_events =  get_option('mobey_past_events');
    
    
    
   ?>

    <table class="form-table">
    <tr valign="top">
        <th scope="row">Small heading </th>
        <td><input type="text" size="160" name="mobey_past_events[small_heading]" value="<?php echo $mobey_past_events['small_heading']; ?>" /></td>
        </tr>
        <tr valign="top">
        <th scope="row">Big heading </th>
        <td><input type="text" size="160" name="mobey_past_events[big_heading]" value="<?php echo $mobey_past_events['big_heading']; ?>" /></td>
        </tr>
        <tr valign="top">
        <th scope="row">Year </th>
        <td><input type="text" size="160" name="mobey_past_events[year]" value="<?php echo $mobey_past_events['year']; ?>" /></td>
        </tr>
        <th scope="row">Top of diamond</th>
        <td><input type="text" size="160" name="mobey_past_events[diamond]" value="<?php echo $mobey_past_events['diamond']; ?>" /></td>
        </tr>
        </tr>
        <th scope="row">Background image url</th>
        <td><input type="text" size="160" name="mobey_past_events[background]" value="<?php echo $mobey_past_events['background']; ?>" /></td>
        </tr>
        
         <tr valign="top">
        <th scope="row">Main text</th>
        <td>
         <?php
         
                         $settings = array(
    'teeny' => false,
    'textarea_rows' => 12,
    'tabindex' => 1,
    'textarea_name' => 'mobey_past_events[past_events_text]'
);
wp_editor($mobey_past_events['past_events_text'], 'past_events_text', $settings);
         ?>
         </td></tr>
        <tr valign="top">
        <th scope="row">Button 1 link text</th>
        <td><input type="text" size="160" name="mobey_past_events[button_1_text]" value="<?php echo $mobey_past_events['button_1_text']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Button 1 link url</th>
        <td><input type="text" size="160" name="mobey_past_events[button_1_link]" value="<?php echo $mobey_past_events['button_1_link']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Button 2 link text</th>
        <td><input type="text" size="160" name="mobey_past_events[button_2_text]" value="<?php echo $mobey_past_events['button_2_text']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Button 2 link url</th>
        <td><input type="text" size="160" name="mobey_past_events[button_2_link]" value="<?php echo $mobey_past_events['button_2_link']; ?>" /></td>
        </tr>
        
       
        
       
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php }  


function mobey_workgroups_settings_page() {
?>
<div class="wrap">
<h1>Workgroups </h1>

<form method="post" action="options.php">
    <?php settings_fields( 'mobey-workgroups-group' ); ?>
    <?php do_settings_sections( 'mobey_workgroups-group' ); ?>
    <?php $mobey_workgroups =  get_option('mobey_workgroups');
    
    
    
   ?>

    <table class="form-table">
          <tr valign="top">
        <th scope="row">Current workgroups box1 heading</th>
        <td><input type="text" size="160" name="mobey_workgroups[box1_heading]" value="<?php echo $mobey_workgroups['box1_heading']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Workgroup button link</th>
        <td><input type="text" size="160" name="mobey_workgroups[box1_link]" value="<?php echo $mobey_workgroups['box1_link']; ?>" /></td>
        </tr>
         <tr valign="top">
        <th scope="row">Current workgroups box1 text</th>
        <td>
         <?php
        
    $settings = array(
    'teeny' => false,
    'textarea_rows' => 12,
    'tabindex' => 1,
    'textarea_name' => 'mobey_workgroups[currentbox_text]'
);
wp_editor($mobey_workgroups['currentbox_text'], 'currentbox_text', $settings);
         ?>
         </td></tr>
        <tr valign="top">
        <th scope="row">Current workgroups box2 heading</th>
        <td><input type="text" size="160" name="mobey_workgroups[box2_heading]" value="<?php echo $mobey_workgroups['box2_heading']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Workgroup button link</th>
        <td><input type="text" size="160" name="mobey_workgroups[box2_link]" value="<?php echo $mobey_workgroups['box2_link']; ?>" /></td>
        </tr>
       <tr valign="top">
        <th scope="row">Current workgroups box2</th>
        <td>
         <?php
        
    $settings = array(
    'teeny' => false,
    'textarea_rows' => 12,
    'tabindex' => 1,
    'textarea_name' => 'mobey_workgroups[currentbox2_text]'
);
wp_editor($mobey_workgroups['currentbox2_text'], 'currentbox2_text', $settings);
         ?>
         </td></tr>
         
                 <tr valign="top">
        <th scope="row">Current workgroups box3 heading</th>
        <td><input type="text" size="160" name="mobey_workgroups[box3_heading]" value="<?php echo $mobey_workgroups['box3_heading']; ?>" /></td>
        </tr>
          <tr valign="top">
        <th scope="row">Workgroup button link</th>
        <td><input type="text" size="160" name="mobey_workgroups[box3_link]" value="<?php echo $mobey_workgroups['box3_link']; ?>" /></td>
        </tr>
       <tr valign="top">
        <th scope="row">Current workgroups box3</th>
        <td>
         <?php
        
    $settings = array(
    'teeny' => false,
    'textarea_rows' => 12,
    'tabindex' => 1,
    'textarea_name' => 'mobey_workgroups[currentbox3_text]'
);
wp_editor($mobey_workgroups['currentbox3_text'], 'currentbox3_text', $settings);
         ?>
         </td></tr>
         
                 <tr valign="top">
        <th scope="row">Current workgroups box4 heading</th>
        <td><input type="text" size="160" name="mobey_workgroups[box4_heading]" value="<?php echo $mobey_workgroups['box4_heading']; ?>" /></td>
        </tr>
        <tr valign="top">
        <th scope="row">Workgroup button link</th>
        <td><input type="text" size="160" name="mobey_workgroups[box4_link]" value="<?php echo $mobey_workgroups['box4_link']; ?>" /></td>
        </tr>
       <tr valign="top">
        <th scope="row">Current workgroups box4</th>
        <td>
         <?php
        
    $settings = array(
    'teeny' => false,
    'textarea_rows' => 12,
    'tabindex' => 1,
    'textarea_name' => 'mobey_workgroups[currentbox4_text]'
);
wp_editor($mobey_workgroups['currentbox4_text'], 'currentbox4_text', $settings);
         ?>
         </td></tr>
      
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php }  


/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

?>

<?php
/**
 * AJAC filter posts by taxonomy term
 */
function vb_filter_posts_mt() {
	if( !isset( $_POST['nonce'] ) || !wp_verify_nonce( $_POST['nonce'], '1234codens' ) )
		die('Permission denied');
	/**
	 * Default response
	 */
	$response = [
		'status'  => 500,
		'message' => 'Something is wrong, please try again later ...',
		'content' => false,
		'found'   => 0
	];
	$all     = false;
	$terms   = $_POST['params']['terms'];
	$page    = intval($_POST['params']['page']);
	$cate    = $_POST['params']['cate'];
	$orderby    = $_POST['params']['sort'];
	$qty     = intval($_POST['params']['qty']);
	$pager   = isset($_POST['pager']) ? $_POST['pager'] : 'pager';
	$tax_qry = [];
	$msg     = '';
	if ($orderby === 'date' )$order="DESC"; else $order="ASC"; 
	
	/**
	 * Check if term exists
	 */
	if (!is_array($terms)) :
		$response = [
			'status'  => 501,
			'message' => 'Term doesn\'t exist',
			'content' => 0
		];
		die(json_encode($response));
	else :
		foreach ($terms as $tax => $slugs) :
			if (in_array('all-terms', $slugs)) {
				$all = true;
			}
			$tax_qry[] = [
				'taxonomy' => $tax,
				'field'    => 'slug',
				'terms'    => $slugs,
			];
		endforeach;
	endif;
	/**
	 * Setup query
	 */
	 if ($cate==0){ $post_type='Videos';} else $post_type='post';
	$args = [
		'paged'          => $page,
		'post_type'      => $post_type,
		'post_status'    => 'publish',
		'posts_per_page' => $qty,
		'cat' => $cate,
		'orderby' => $orderby,
		'order' => $order,
	];
	if ($tax_qry && !$all) :
		$args['tax_query'] = $tax_qry;
	endif;
	$qry = new WP_Query($args);
	ob_start();
	$i=0;
	
		if ($qry->have_posts()) :
			while ($qry->have_posts()) : $qry->the_post(); ?>

				<article class="loop-item">
				 <?php
         if ($cate ==0){ 
         
                  print "<div class=\"vw-container\"><div class='video-thumbnail'>";
         $i++; 
         $custom = get_post_custom($post->ID);
          $youtube_url= $custom["youtube-url"][0];
         print "<a class=\"read-more\" target=\"_blank\" href=\"$youtube_url\"><div class=\"play\"></div>";
         
          if ( has_post_thumbnail() ) {
         the_post_thumbnail( 'medium' );
} 
          ?></a></div><div class="content">
        <div class="video-indicator">Video</div>
        <div class="heading-div-db"><span class="news-heading"><?php the_title(); ?></span></div>
        <div class="blogs-date"><?php print get_the_date('jS \o\f F Y'); ?>
        <div class="blog-link"><a class="read-more" target="_blank" href="<?php print $youtube_url; ?>">Watch now</a></div>
        </div>
        
      </div>
      
   </div>
   <?php
         
          
          }
          else{
         print "<div class=\"pb-reports-container\">";
         $i++; 
         
         if (str_word_count (get_the_title())<5)$long_excerpt=true; else $long_excerpt=false;
         if (str_word_count (get_the_title())>10)$no_content=true; else $no_content=false;
         
         
          if ( has_post_thumbnail() ) {
         print "<div class='reports-thumbnail'>";
         the_post_thumbnail( 'reports' );
         print "</div>";
} 
          ?>
          <div class="blog-content-right"> 
        <?php if ($cate ==4) print '<div class="blog-indicator">Blog</div>'; 
        if ($cate ==98) print '<div class="report-indicator">Report</div>';
        if ($cate ==7) print '<div class="press-indicator">Press release</div>';
        if ($cate ==149) print '<div class="webinar-indicator">Webinar</div>';
        
         ?>
         
        <div class=" blog-heading-div"><a class="news-heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
        <?php if ($long_excerpt==true) { ?> <div class="blogs-content"><?php html5wp_excerpt(30); ?></div> <?php } ?>

        <?php if ($long_excerpt==false && $no_content==false) { ?> <div class="blogs-content"><?php html5wp_excerpt(20); ?></div> <?php } ?>
        <div class="blogs-date"><?php print get_the_date('jS \o\f F Y'); ?>
              <div class="blog-link"><a class="read-more" href="<?php the_permalink(); ?>">Read more </a></div>
        </div>
      </div>
   </div>
   <div class="clear"></div>
   <?php } ?>
   
   </article>
			<?php endwhile;
			/**
			 * Pagination
			 */
			if ( $pager == 'pager' )
				vb_mt_ajax_pager($qry,$page);
			foreach ($tax_qry as $tax) :
				$msg .= 'Displaying terms: ';
				foreach ($tax['terms'] as $trm) :
					$msg .= $trm . ', ';
				endforeach;
				$msg .= ' from taxonomy: ' . $tax['taxonomy'];
				$msg .= '. Found: ' . $qry->found_posts . ' posts';
			endforeach;
			$response = [
				'status'  => 200,
				'found'   => $qry->found_posts,
				'message' => $msg,
				'method'  => $pager,
				'next'    => $page + 1
			];
			
		else :
			$response = [
				'status'  => 201,
				'message' => 'No posts found',
				'next'    => 0
			];
		endif;
	$response['content'] = ob_get_clean();
	die(json_encode($response));
}
add_action('wp_ajax_do_filter_posts_mt', 'vb_filter_posts_mt');
add_action('wp_ajax_nopriv_do_filter_posts_mt', 'vb_filter_posts_mt');
/**
 * Shortocde for displaying terms filter and results on page
 */
function vb_filter_posts_mt_sc($atts) {
	$a = shortcode_atts( array(
		'tax'      => 'post_tag', // Taxonomy
		'terms'    => false, // Get specific taxonomy terms only
		'active'   => true, // Set active term by ID
		'per_page' => 12, // How many posts per page,
		'pager'    => 'pager', // 'pager' to use numbered pagination || 'infscr' to use infinite scroll
		'cat'    => ''
	), $atts );
	$result = NULL;
	$terms  = get_terms($a['tax']);
	$cat_slug = $atts['cat'];
	if ($cat_slug ==0)$cat_slug = 179;
	$name = get_the_category_by_ID($cat_slug); 
    
	
	if (count($terms)) :
		ob_start(); ?>
			<div id="container-async" data-cat="<?= $a['cat']; ?>" data-paged="<?= $a['per_page']; ?>" class="sc-ajax-filter sc-ajax-filter-multi">
				<div class="sidebar-half">
				<h3><?php print $name; ?></h3>
				Sort by
				<ul class="nav-filter">
          <li>
              <a href="#" data-sort="date" data-page="1">date newest first</a>
          </li> 
         <li>
              <a href="#" data-sort="title" data-page="1">alphabetically a-z</a>
          </li>    
				</ul>
        <!--Tags <a href="#" class="all-tags">select all</a>-->
				<ul id="tag-filter" class="nav-filter">
					<li>
					
					<a href="#" class="hidden show-all-link" data-filter="<?= $terms[0]->taxonomy; ?>" data-term="all-terms" data-page="1">
							Show All
						</a>
					</li></ul><!--
					<?php foreach ($terms as $term) : ?>
						<li<?php if ($term->term_id == $a['active']) :?> class="active"<?php endif; ?>>
							<a href="<?= get_term_link( $term, $term->taxonomy ); ?>" data-filter="<?= $term->taxonomy; ?>" data-term="<?= $term->slug; ?>" data-page="1">
								<?= $term->name; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>-->
       </div>
       <div class="content-half">
				<div class="intro"> <?php echo category_description( $cat_slug  ); ?> </div>
				<div class="content"></div>
      
				<?php if ( $a['pager'] == 'infscr' ) : ?>
					<nav class="pagination infscr-pager">
						<a href="#page-2" class="btn btn-primary">Load More</a>
					</nav>
				<?php endif; ?>
			 </div>
			 <div class="clear"></div>
		</div>
		<?php $result = ob_get_clean();
	endif;
	return $result;
}
add_shortcode( 'ajax_filter_posts_mt', 'vb_filter_posts_mt_sc');
/**
 * Pagination
 */
function vb_mt_ajax_pager( $query = null, $paged = 1 ) {
	if (!$query)
		return;
	$paginate = paginate_links([
		'base'      => '%_%',
		'type'      => 'array',
		'total'     => $query->max_num_pages,
		'format'    => '#page=%#%',
		'current'   => max( 1, $paged ),
		'prev_text' => 'Prev',
		'next_text' => 'Next'
	]);
	if ($query->max_num_pages > 1) : ?>
		<ul class="pagination">
			<?php foreach ( $paginate as $page ) :?>
				<li><?php echo $page; ?></li>
			<?php endforeach; ?>
		</ul>
	<?php endif;
}
?>



<?php

/**
 * HERE IS stuff from old site
 *
 */
function presscore_display_post_author() {

    $user_url = get_the_author_meta('user_url');

    if (1 || dt_validate_gravatar(get_the_author_meta('user_email'))) {
        $avatar = get_avatar(get_the_author_meta('ID'), 85, presscore_get_default_avatar());
    } else {
        $avatar = '';
    }
    ?>

    <div class="dt-fancy-separator title-left fancy-author-title">
        <div class="dt-fancy-title"><?php _e('About the author', 'the7mk2'); ?><span class="separator-holder separator-right"></span></div>
    </div>


    <?php
}

function presscore_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;

    switch ($comment->comment_type) :
        case 'pingback' :
        case 'trackback' :
            ?>
            <li class="pingback">
                <div class="pingback-content">
                    <span><?php _e('Pingback:', 'the7mk2'); ?></span>
                    <?php comment_author_link(); ?>
                    <?php edit_comment_link(__('(Edit)', 'the7mk2'), ' '); ?>
                </div>
                <?php
                break;
            default :
                ?>
            <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">

                <article id="div-comment-<?php comment_ID(); ?>">

                    <div class="reply">
                        <?php comment_reply_link(array_merge($args, array('add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                    </div><!-- .reply -->

                    <div class="comment-meta">
                        <time datetime="<?php comment_time('c'); ?>">
                            <?php
                            /* translators: 1: date, 2: time */
                            // TODO: add date/time format (for qTranslate)
                            printf(__('%1$s at %2$s', 'the7mk2'), get_comment_date(), get_comment_time());
                            ?>
                        </time>
                        <?php edit_comment_link(__('(Edit)', 'the7mk2'), ' '); ?>
                    </div><!-- .comment-meta -->

                    <div class="comment-author vcard">
                        <?php
                        echo '7777 dssdf';
                        if (1 || dt_validate_gravatar($comment->comment_author_email)) {
                            echo '7777 dssdf';
                            $avatar = get_avatar($comment, 60);
                        } else {
                            $avatar = '<span class="avatar no-avatar"></span>';
                        }

                        $author_url = get_comment_author_url();
                        if (empty($author_url) || 'http://' == $author_url) {
                            echo $avatar;
                        } else {
                            echo '<a href="' . $author_url . '" rel="external nofollow" class="rollover" target="_blank">' . $avatar . '</a>';
                        }

                        printf('<cite class="fn">%s</cite>', str_replace('href', 'target="_blank" href', get_comment_author_link()));
                        ?>
                    </div><!-- .comment-author .vcard -->

                    <?php if ($comment->comment_approved == '0') : ?>
                        <em><?php _e('Your comment is awaiting moderation.', 'the7mk2'); ?></em>
                        <br />
                    <?php endif; ?>

                    <div class="comment-content"><?php comment_text(); ?></div>

                </article>

                <?php
                break;
        endswitch;
    }

    class Presscore_Inc_Logos_Post_Type {

        public static $post_type = 'dt_logos';
        public static $taxonomy = 'dt_logos_category';
        public static $menu_position = 41;

        public static function register() {

            // titles
            $labels = array(
                'name' => _x('Partners, Clients, etc.', 'backend logos', 'the7mk2'),
                'singular_name' => _x('Partner,Client, etc.', 'backend logos', 'the7mk2'),
                'add_new' => _x('Add New Logo', 'backend logos', 'the7mk2'),
                'add_new_item' => _x('Add New Logo', 'backend logos', 'the7mk2'),
                'edit_item' => _x('Edit Partner,Client, etc.', 'backend logos', 'the7mk2'),
                'new_item' => _x('New Item', 'backend logos', 'the7mk2'),
                'view_item' => _x('View Item', 'backend logos', 'the7mk2'),
                'search_items' => _x('Search Items', 'backend logos', 'the7mk2'),
                'not_found' => _x('No items found', 'backend logos', 'the7mk2'),
                'not_found_in_trash' => _x('No items found in Trash', 'backend logos', 'the7mk2'),
                'parent_item_colon' => '',
                'menu_name' => _x('Partners, Clients, etc.', 'backend logos', 'the7mk2')
            );

            // options
            $args = array(
                'labels' => $labels,
                'public' => true,
                'publicly_queryable' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => true,
                'capability_type' => 'post',
                'has_archive' => true,
                'hierarchical' => false,
                'menu_position' => self::$menu_position,
                'supports' => array('title', 'thumbnail')
            );

            $args = apply_filters('presscore_post_type_' . self::$post_type . '_args', $args);

            register_post_type(self::$post_type, $args);
            /* post type end */

            /* setup taxonomy */

            // titles
            $labels = array(
                'name' => _x('Logo Categories', 'backend partners', 'the7mk2'),
                'singular_name' => _x('Logo Category', 'backend partners', 'the7mk2'),
                'search_items' => _x('Search in Category', 'backend partners', 'the7mk2'),
                'all_items' => _x('Logo Categories', 'backend partners', 'the7mk2'),
                'parent_item' => _x('Parent Category', 'backend partners', 'the7mk2'),
                'parent_item_colon' => _x('Parent Category:', 'backend partners', 'the7mk2'),
                'edit_item' => _x('Edit Category', 'backend partners', 'the7mk2'),
                'update_item' => _x('Update Category', 'backend partners', 'the7mk2'),
                'add_new_item' => _x('Add New Logo Category', 'backend partners', 'the7mk2'),
                'new_item_name' => _x('New Logo Category Name', 'backend partners', 'the7mk2'),
                'menu_name' => _x('Logo Categories', 'backend partners', 'the7mk2')
            );

            $taxonomy_args = array(
                'hierarchical' => true,
                'public' => true,
                'labels' => $labels,
                'show_ui' => true,
                'rewrite' => true,
                'show_admin_column' => true,
            );

            $taxonomy_args = apply_filters('presscore_taxonomy_' . self::$taxonomy . '_args', $taxonomy_args);

            register_taxonomy(self::$taxonomy, array(self::$post_type), $taxonomy_args);
            /* taxonomy end */
        }

    }

    add_filter('gform_field_value_uniqueOrderID', 'generateUniqueOrderID');

    function generateUniqueOrderID() {
        return uniqid();
    }

    add_action('wp_ajax_pt_generate_auth', 'pt_generate_auth');
    add_action('wp_ajax_nopriv_pt_generate_auth', 'pt_generate_auth');

    function pt_generate_auth() {
        $merchantAuthHash = get_option('pt_merchant_auth');
        $merchantID = get_option('pt_merchant_id');
        $amount = $_POST['amount'];
        $orderNr = $_POST['orderNr'];
        $orderDesc = $_POST['orderDescription'];
        $currencyCode = get_option('pt_currency_code');
        $returnAddress = $_POST['returnAddress'];
        $cancelAddress = $_POST['cancelAddress'];
        $transactionType = "S1";
        $language = "en_US";

        // Auth hash fields:
        // merchAuthHash|MERCHANT_ID|AMOUNT|ORDER_NUMBER|REFERENCE_NUMBER|ORDER_DESCRIPTION|CURRENCY|RETURN_ADDRESS|CANCEL_ADDRESS|
        // PENDING_ADDRESS|NOTIFY_ADDRESS|TYPE|CULTURE|PRESELECTED_METHOD|MODE|VISIBLE_METHODS|GROUP

        $authStr = "{$merchantAuthHash}|{$merchantID}|{$amount}|{$orderNr}||{$orderDesc}|{$currencyCode}|{$returnAddress}|{$cancelAddress}|||{$transactionType}|{$language}||||";
        $mac = strtoupper(hash("md5", $authStr));
        echo $mac;
        die();
    }

    function mobey_day_emvco_payment_paytrail_shortcode($atts) {
        $atts = shortcode_atts(
                array(), $atts
        );

        $customerFirstName = $customerLastName = $customerEmail = $customerCompanyName = $customerOrderNr = $discountPrice = null;
        if (isset($_GET['firstname']) && isset($_GET['lastname']) &&
                isset($_GET['companyname']) && isset($_GET['ordernr'])) {
            $customerFirstName = $_GET['firstname'];
            $customerLastName = $_GET['lastname'];
            $customerCompanyName = $_GET['companyname'];
            $customerEmail = $_GET['email'];
            $customerOrderNr = $_GET['ordernr'];
        }

        if (isset($_GET['discountApplied']) && isset($_GET['discountPrice'])) {
            $discountPrice = $_GET['discountPrice'];
        }

        $customerDefaultCost = "302,50";

        $responseMessage = '';
        if (isset($_GET['response']) && $_GET['response'] != '') {
            switch ($_GET['response']) {
                case 'success':
                    $responseMessage .= "<span style='color:#0A6AA2;'>Payment has been successfully sent.<br/> An email has been sent to {$customerEmail} with the receipt</span>";
                    $emailTemplateHeader = "Dear {$customerFirstName},\n\n";
                    $emailTemplateHeader .= "Many thanks for your registration to the EMVco Seminar on October 15 in Barcelona.\n\nYour payment has been processed a receipt will be sent to you within a few days.\n\n";
                    $emailTemplateHeader .= "1 Pass to EMVco Seminar\n\n";
                    $emailTemplateFooter = "If you have any questions, please contact us\n\n";
                    $emailTemplateFooter .= "Kind regards,\n\n";
                    $emailTemplateFooter .= "Laura Sanguinetti\n";
                    $emailTemplateFooter .= "laura.sanguinetti@mobeyforum.org\nTel +358 40 355 1983";
                    $emailTemplateWithDiscount = "{$emailTemplateHeader}Price\n";
                    $emailTemplateWithDiscount .= "€ {$discountPrice}\n\n\n{$emailTemplateFooter}";
                    $emailTemplateNoDiscount = "{$emailTemplateHeader}Price \n";
                    $emailTemplateNoDiscount .= "€ 302,50\n\n\n{$emailTemplateFooter}";
                    $emailContent = $emailTemplateNoDiscount;
                    if ($_GET['discountApplied']) {
                        $emailContent = $emailTemplateWithDiscount;
                    }
                    if ($customerEmail !== null) {
                        $headers = "From: Mobey Forum <mobeyforum@mobeyforum.org>\r\n";
                        wp_mail($customerEmail, "Receipt for Mobey Day EMVco Seminar Payment", $emailContent, $headers);
                    }
                    break;
                case 'cancel':
                    $responseMessage .="<span style='color:red'>Payment cancelled!</span>";
                    break;
                default:
                    break;
            }
        }
        ob_start();
        ?>

        <script type="text/javascript">
            var pageUrl = '<?php echo home_url('/mobey-day-emvco-seminar-payment'); ?>';
            var responsePageUrl = pageUrl + "?response";
            var costValue = parseFloat('<?php echo str_replace(",", ".", $customerDefaultCost); ?>');

            function SetFields() {
                var customerFirstName = jQuery('#Customer_First_Name').val();
                var customerLastName = jQuery('#Customer_Last_Name').val();
                var orderNr = jQuery('#Order_Number').val();
                var amountForm = jQuery('#AmountForm').val();
                var companyName = ', Company Name: ' + jQuery('#Company_Name').val();

                //Validate
                if (jQuery.trim(customerFirstName) === '' || jQuery.trim(customerLastName) === '' ||
                        jQuery.trim(orderNr) === '' || jQuery.trim(amountForm) === '') {
                    jQuery('#message-warning-item').html('All fields are required!');
                    jQuery('#message-warning-box').css('display', 'block');
                    return false;
                }

                var amount = amountForm.replace(",", ".");
                amount = parseFloat(amount);
                if (isNaN(amount)) {
                    jQuery('#message-warning-item').html('Invalid Amount!');
                    jQuery('#message-warning-box').css('display', 'block');
                    return false;
                }

                var discountApplied = false;

                if (amount !== costValue) {
                    discountApplied = true;
                }

                amount = amount.toFixed(2);
                jQuery('#Amount').val(amount);

                jQuery('#message-warning-box').css('display', 'none');



                //Populate fields
                var orderDescription = encodeURI('Customer:' + customerFirstName + ' ' + customerLastName + companyName + ', Reference number:' + orderNr + ', Amount:' + amountForm);
                orderDescription = orderDescription.replace(/'/g, "%27");
                jQuery('#Order_Description').val(orderDescription);

                var email = jQuery('#Customer_Email').val();
                var responseParameters = encodeURI('&ordernr=' + orderNr + '&firstname=' + customerFirstName + '&lastname=' + customerLastName + '&companyname=' + jQuery('#Company_Name').val() + '&email=' + email);
                responseParameters = responseParameters.replace(/'/g, "%27");

                if (discountApplied) {
                    responseParameters += "&discountApplied=yes&discountPrice=" + parseInt(amount);
                }
                jQuery('#Success_Url').val(responsePageUrl + '=success' + responseParameters);
                jQuery('#Cancel_Url').val(responsePageUrl + '=cancel' + responseParameters);

                if (discountApplied && amount <= 0) {
                    window.location.href = responsePageUrl + '=success' + responseParameters;
                    return false;
                }

                var data = {
                    action: 'pt_generate_auth',
                    amount: amount,
                    orderNr: orderNr,
                    returnAddress: jQuery('#Success_Url').val(),
                    cancelAddress: jQuery('#Cancel_Url').val(),
                    orderDescription: jQuery('#Order_Description').val()
                };

                jQuery.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    data: data,
                    success: function (result) {
                        jQuery('#paytrail-auth-code').val(result);
                        jQuery('#ptform').submit();
                    }
                });
            }
            jQuery(document).ready(function ($) {
    <?php
    if ($_GET['response'] == "success") {
        echo "$('input[name=\"SendPaymentButton\"]').attr('disabled', 'disabled');\n";
        echo "$('#hideFieldsDiv').css('display', 'none');\n";
    }
    ?>
                $("#discountCode").change(function () {
                    if ($(this).val() === "emvcosub") {
                        $("#AmountForm").val("181,50");
                    } else if ($(this).val() === "emvcoassoc") {
                        $("#AmountForm").val("0");
                    } else {
                        $("#AmountForm").val(costValue);
                    }
                });
            });
        </script>

        <form id="ptform" action="https://payment.paytrail.com/" methos="post">
            <div class="validation_error" id="message-warning-box" style="display:none;" >
                <ul>
                    <li id="message-warning-item"></li>
                </ul>
            </div>
            <?php if (isset($responseMessage) && $responseMessage != '') {
                ?>
                <div class="validation_error">
                    <?php echo $responseMessage; ?>
                </div>
            <?php } ?>

            <div id="hideFieldsDiv">
                <h4>Reference number:</h4>
                <div>
                    <input readonly="readonly" type="text" name="ORDER_NUMBER" id="Order_Number" value="<?php echo $customerOrderNr; ?>" />
                </div>
                <h4>Discount Code</h4>
                <div>
                    <input type="text" id="discountCode" value="" />
                </div>
                <h4>Amount</h4>
                <div>
                    <input readonly="readonly" type="text" id="AmountForm" value="<?php echo $customerDefaultCost; ?>" />
                    <input type="hidden" name="AMOUNT" id="Amount" value="" /> <?php echo get_option('pt_currency_symbol'); ?>
                </div>

                <h4>First Name</h4>
                <div>
                    <input type="text" id="Customer_First_Name" value="<?php echo $customerFirstName; ?>"  />
                </div>

                <h4>Last Name</h4>
                <div>
                    <input type="text" id="Customer_Last_Name" value="<?php echo $customerLastName; ?>"  />
                </div>

                <h4>Email</h4>
                <div>
                    <input type="text" id="Customer_Email" value="<?php echo $customerEmail; ?>"  />
                </div>

                <h4>Company Name</h4>
                <div>
                    <input type="text" id="Company_Name" value="<?php echo $customerCompanyName; ?>"  />
                </div>

                <div>
                    <input type="hidden" name="AUTHCODE" value="<?php echo get_option('pt_merchant_auth'); ?>" />
                    <input type="hidden" name="MERCHANT_ID" value="<?php echo get_option('pt_merchant_id'); ?>" />
                    <!-- <input name="REFERENCE_NUMBER" type="hidden" value=""> -->
                    <input type="hidden" name="ORDER_DESCRIPTION" value="" id="Order_Description">
                    <input type="hidden" name="CURRENCY" value="<?php echo get_option('pt_currency_code'); ?>" /> <!-- Only EUR is accepted for Finnish banks and credit cards. -->
                    <input type="hidden" name="RETURN_ADDRESS" id="Success_Url" value="" />
                    <input type="hidden" name="CANCEL_ADDRESS" id="Cancel_Url" value="" />
                    <!-- <input name="PENDING_ADDRESS" type="hidden" value=""> -->
                    <!-- <input name="NOTIFY_ADDRESS" type="hidden" value=""> -->
                    <input type="hidden" name="TYPE" value="S1" />
                    <input type="hidden" name="CULTURE" value="en_US">
                    <!-- <input name="PRESELECTED_METHOD" type="hidden" value=""> -->
                    <!-- <input name="MODE" type="hidden" value="1"> -->
                    <!-- <input name="VISIBLE_METHODS" type="hidden" value=""> -->
                    <!-- <input name="GROUP" type="hidden" value=""> -->
                    <input type="hidden" name="AUTHCODE" id="paytrail-auth-code" value="">

                    <input type="button" class="button" onclick="SetFields();" name="SendPaymentButton" value="Send" />

                </div>
                <br/>
                <div class="payment-service-info">
                    <p>Payment service provider</p>
                    <br/>
                    <p>Paytrail Oyj (2122839-7) acts as an implementer of the payment handling service and as a Payment Service Provider. Paytrail Oyj will be shown as the recipient in the invoice and Paytrail Oyj will forward the payment to the merchant.</p>
                    <p>Paytrail Oyj is an authorized Payment Institution. For reclamations, please contact the website you made your payment to.</p>
                    <br/>
                    <p>Paytrail Oyj, business ID 2122839-7</p>
                    <p>Innova 2</p>
                    <p>Lutakonaukio 7<p>
                    <p>40100 Jyväskylä</p>
                    <p>Phone: +358 207 181830</p>
                    <br/>
                    <p>Netbanks</p>
                    <br/>
                    <p>Paytrail Oyj (FI2122839) provides netbank related payment transfer services in co-operation with Finnish banks and credit institutions. For consumer the service works exactly the same way as traditional web payments.</p>
                </div>
            </div>
        </form>

        <?php
        return ob_get_clean();
    }

    add_shortcode('mobey-day-emvco-paytrail-payment', 'mobey_day_emvco_payment_paytrail_shortcode');

    function paytrail_payment() {
        if (isset($_POST['pt_options_submitted']) && $_POST['pt_options_submitted'] == 'yes') {
            update_option("pt_merchant_id", stripslashes($_POST['pt_merchant_id']));
            update_option("pt_merchant_auth", stripslashes($_POST['pt_merchant_auth']));
            update_option("pt_currency_symbol", stripslashes($_POST['pt_currency_symbol']));
            update_option("pt_currency_code", stripslashes($_POST['pt_currency_code']));
            echo "<div id=\"message\" class=\"updated fade\"><p><strong>Your settings have been saved.</strong></p></div>";
        }
        ?>

        <div class="wrap">
            <h2>Paytrail Payment Settings</h2>
            <div class="settings_container" style="width: 100%; margin-right: -200px; float: left;">
                <div style="margin-right: 200px;">
                    <form method="post" name="pt_options_form" target="_self">
                        <table class="form-table">
                            <tr valign="top">
                                <th>Paytrail Merchant id</th>
                                <td><input type="text" name="pt_merchant_id" value="<?php echo get_option('pt_merchant_id'); ?>" /></td>
                            <tr>
                            <tr valign="top">
                                <th>Paytrail Merchant Auth</th>
                                <td><input type="text" name="pt_merchant_auth" value="<?php echo get_option('pt_merchant_auth'); ?>" /></td>
                            </tr>
                            <tr valign="top">
                                <th>Currency Symbol</th>
                                <td><input type="text" name="pt_currency_symbol" value="<?php echo get_option('pt_currency_symbol'); ?>" /></td>
                            </tr>
                            <tr valign="top">
                                <th>Currency Code</th>
                                <td><input type="text" name="pt_currency_code" value="<?php echo get_option('pt_currency_code'); ?>" /></td>
                            </tr>
                            <tr valign="top">
                                <td>
                                    <input name="pt_options_submitted" type="hidden" value="yes" />
                                    <input type="submit" name="Submit" value="Save Changes" />
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <?php
    }

    add_action('admin_menu', 'paytrail_payment_options');

    function paytrail_payment_options() {
        add_submenu_page('options-general.php', 'Paytrail Payment Settings', 'Paytrail Payment Settings', 'manage_options', 'pt-settings', 'paytrail_payment');
    }

    function membership_payment_shortcode($atts) {
        $atts = shortcode_atts(
                array(), $atts
        );

        global $current_user;
        get_currentuserinfo();
        $customerID = $current_user->ID;
        if (isset($_GET['firstname']) && isset($_GET['lastname']) && isset($_GET['companyname'])) {
            $customerFirstName = $_GET['firstname'];
            $customerLastName = $_GET['lastname'];
            $customerCompanyName = $_GET['companyname'];
        } else {
            $customerFirstName = $current_user->user_firstname;
            $customerLastName = $current_user->user_lastname;
        }
        if ($customerID == '') {
            $customerID = 'anonymous' . time();
        }

        $responseMessage = '';
        if (isset($_GET['response']) && $_GET['response'] != '') {
            switch ($_GET['response']) {
                case 'success':
                    $responseMessage .="<span style='color:#0A6AA2;'>Payment has been successfully sent. Reference number: {$_REQUEST['orderID']}</span>";
                    break;
                case 'cancel':
                    $responseMessage .="<span style='color:red'>Payment cancelled!</span>";
                    break;
                default:
                    break;
            }
        }
        ob_start();
        ?>

        <script type="text/javascript">
            <!--
        var pageUrl = '<?php echo home_url('/membership-payment'); ?>';
            var responsePageUrl = pageUrl + "?response";

            function SetFields() {
                var companyName = jQuery('#Company_Name').val();
                var customerFirstName = jQuery('#Customer_First_Name').val();
                var customerLastName = jQuery('#Customer_Last_Name').val();
                var orderID = jQuery('#Order_ID').val();
                var amountForm = jQuery('#AmountForm').val();
    <?php
    if (isset($_GET['companyname'])) {
        echo "companyName = ', Company Name: ' + jQuery('#Company_Name').val();";
    }
    ?>

                //Validate
                if (jQuery.trim(customerFirstName) === '' || jQuery.trim(customerLastName) === '' ||
                        jQuery.trim(orderID) === '' || jQuery.trim(amountForm) === '') {
                    jQuery('#message-warning-item').html('All fields are required!');
                    jQuery('#message-warning-box').css('display', 'block');
                    return false;
                }

                var amount = amountForm.replace(",", ".");
                amount = parseFloat(amount);
                if (isNaN(amount)) {
                    jQuery('#message-warning-item').html('Invalid Amount!');
                    jQuery('#message-warning-box').css('display', 'block');
                    return false;
                }
                amount = amount.toFixed(2);
                jQuery('#Amount').val(amount);

                jQuery('#message-warning-box').css('display', 'none');


                //Populate fields
                var orderDescription = encodeURI('Company:' + companyName + ' Customer:' + customerFirstName + ' ' + customerLastName + ', Reference number:' + orderID + ', Amount:' + amountForm);
                orderDescription = orderDescription.replace(/'/g, "%27");
                jQuery('#Order_Description').val(orderDescription);

                jQuery('#Success_Url').val(responsePageUrl + '=success' + encodeURI('&orderID=' + orderID) );
                jQuery('#Cancel_Url').val(responsePageUrl + '=cancel' + encodeURI('orderID=' + orderID) );
                
    <?php
    if (isset($_GET['companyname'])) {
        ?>
        var responseParameters = encodeURI('&orderID='+orderID+'&firstname=' + customerFirstName + '&lastname=' + customerLastName + '&companyname=' + jQuery('#Company_Name').val());
        responseParameters = responseParameters.replace(/'/g, "%27");
        jQuery('#Success_Url').val(responsePageUrl + '=success' + responseParameters);
        jQuery('#Cancel_Url').val(responsePageUrl + '=cancel' + responseParameters);
        <?php
    }
    ?>

                var data = {
                    action: 'pt_generate_auth',
                    amount: amount,
                    orderNr: orderID,
                    returnAddress: jQuery('#Success_Url').val(),
                    cancelAddress: jQuery('#Cancel_Url').val(),
                    orderDescription: jQuery('#Order_Description').val()
                };

                jQuery.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    data: data,
                    success: function (result) {
                        jQuery('#paytrail-auth-code').val(result);
                        jQuery('#ptform').submit();
                    }
                });
            }
            // -->
        </script>

        <form id="ptform" action="https://payment.paytrail.com/" methos="post">
            <div class="validation_error" id="message-warning-box" style="display:none;" >
                <ul>
                    <li id="message-warning-item"></li>
                </ul>
            </div>
            <?php if (isset($responseMessage) && $responseMessage != '') { ?>
                <div class="validation_error">
                    <?php echo $responseMessage; ?>
                </div>
            <?php } ?>
            <p>
            <b>Company Name</b><br>
               
                    <input type="text" id="Company_Name" value="<?php echo $customerCompanyName; ?>"  />
              
            </p>
            <p>
            <b>Reference number:</b><br>
            
                <input type="text" name="ORDER_NUMBER" id="Order_ID" value="" />
            
            </p>
            <p>
            <b>Amount</b><br>
            
                <input type="text" id="AmountForm" value="" />
                <input type="hidden" name="AMOUNT" id="Amount" value="" /> <?php echo get_option('pt_currency_symbol'); ?>
            </p>
            <p>
            
            <b>First Name</b><br>
            
                <input type="text" id="Customer_First_Name" value="<?php echo $customerFirstName; ?>"  />
            </p>
            <p>

            <b>Last Name</b><br>
            
                <input type="text" id="Customer_Last_Name" value="<?php echo $customerLastName; ?>"  />
            </p>
            <p>

            <?php if (isset($_GET['companyname'])) { ?>
            <p>
                <b>Company Name</b><br>
               
                    <input type="text" id="Company_Name" value="<?php echo $_GET['companyname']; ?>"  />
                </p>
            <?php } ?>

            <div>
                <input type="hidden" name="AUTHCODE" value="<?php echo get_option('pt_merchant_auth'); ?>" />
                <input type="hidden" name="MERCHANT_ID" value="<?php echo get_option('pt_merchant_id'); ?>" />
                <!-- <input name="REFERENCE_NUMBER" type="hidden" value=""> -->
                <input type="hidden" name="ORDER_DESCRIPTION" value="" id="Order_Description">
                <input type="hidden" name="CURRENCY" value="<?php echo get_option('pt_currency_code'); ?>" /> <!-- Only EUR is accepted for Finnish banks and credit cards. -->
                <input type="hidden" name="RETURN_ADDRESS" id="Success_Url" value="" />
                <input type="hidden" name="CANCEL_ADDRESS" id="Cancel_Url" value="" />
                <!-- <input name="PENDING_ADDRESS" type="hidden" value=""> -->
                <!-- <input name="NOTIFY_ADDRESS" type="hidden" value=""> -->
                <input type="hidden" name="TYPE" value="S1" />
                <input type="hidden" name="CULTURE" value="en_US">
                <!-- <input name="PRESELECTED_METHOD" type="hidden" value=""> -->
                <!-- <input name="MODE" type="hidden" value="1"> -->
                <!-- <input name="VISIBLE_METHODS" type="hidden" value=""> -->
                <!-- <input name="GROUP" type="hidden" value=""> -->
                <input type="hidden" name="AUTHCODE" id="paytrail-auth-code" value="">
                <p>
                <input type="button" class="button" onclick="SetFields();" name="SendPaymentButton" value="Send" />
                </p> 
            </div>
            <br/>
            <div class="payment-service-info">
                <p>Payment service provider</p>
                <br/>
                <p>Paytrail Oyj (2122839-7) acts as an implementer of the payment handling service and as a Payment Service Provider. Paytrail Oyj will be shown as the recipient in the invoice and Paytrail Oyj will forward the payment to the merchant.</p>
                <p>Paytrail Oyj is an authorized Payment Institution. For reclamations, please contact the website you made your payment to.</p>
                <br/>
                <p>Paytrail Oyj, business ID 2122839-7</p>
                <p>Innova 2</p>
                <p>Lutakonaukio 7<p>
                <p>40100 Jyväskylä</p>
                <p>Phone: +358 207 181830</p>
                <br/>
                <p>Netbanks</p>
                <br/>
                <p>Paytrail Oyj (FI2122839) provides netbank related payment transfer services in co-operation with Finnish banks and credit institutions. For consumer the service works exactly the same way as traditional web payments.</p>
            </div>
        </form>

        <?php
        return ob_get_clean();
    }

    add_shortcode('membership-payment', 'membership_payment_shortcode');

    function calculatePrices($base, $discount, $vatPercent){
        $price = round( $base - ($discount/100 * $base), 1, PHP_ROUND_HALF_UP);
        $vat = round( ($vatPercent/100 * $price), 2, PHP_ROUND_HALF_UP);
        $priceWV = $price - $vat;
        return array('price' => $price, 'vat' => $vat, 'priceWV' => $priceWV);
    }

    function mobey_day_payment_shortcode($atts) {
        $atts = shortcode_atts(
                array(), $atts
        );

        $customerFirstName = $customerLastName = $customerEmail = $customerCompanyName = $customerOrderNr = null;
        if (isset($_GET['firstname']) && isset($_GET['lastname']) &&
                isset($_GET['companyname']) && isset($_GET['ordernr'])) {
            $customerFirstName = $_GET['firstname'];
            $customerLastName = $_GET['lastname'];
            $customerCompanyName = $_GET['companyname'];
            $customerEmail = $_GET['email'];
            $customerOrderNr = $_GET['ordernr'];
        }
        $vatPercent = 21;
        $customerDefaultCost = 1238.50;
        $price = $customerDefaultCost;
        $vat = round( ($vatPercent/100 * $price), 2, PHP_ROUND_HALF_UP);
        $priceWV = $price - $vat;
        $discount = 0;

        $d1Start = mktime(0, 0, 1, 8, 30, date ('Y'));
        $d1End = mktime(23, 59, 59, 9, 11, date ('Y'));
        $d2Start = mktime(0, 0, 1, 9, 12, date ('Y'));
        $d2End = mktime(23, 59, 59, 9, 25, date ('Y'));
        $now = time();
        if ( $now >= $d1Start && $now <= $d1End){
            $discount = 25;
            $pricesValues = calculatePrices($customerDefaultCost, $discount, $vatPercent);
            $price = $pricesValues['price'];
            $vat = $pricesValues['vat'];
            $priceWV = $pricesValues['priceWV'];
        }
        elseif ( $now >= $d2Start && $now <= $d2End){
            $discount = 15;
            $pricesValues = calculatePrices($customerDefaultCost, $discount, $vatPercent);
            $price = $pricesValues['price'];
            $vat = $pricesValues['vat'];
            $priceWV = $pricesValues['priceWV'];
        }

        $banksPrice = 500;
        $banksVat = round( ($vatPercent/100 * $banksPrice), 1, PHP_ROUND_HALF_UP);;
        $banksPriceWV = $banksPrice - $banksVat;

        $partnersDiscount = 25;
        $partnersPricesValues = calculatePrices($customerDefaultCost, $partnersDiscount, $vatPercent);
        $partnersPrice = $partnersPricesValues['price'];
        $partnersVat = $partnersPricesValues['vat'];
        $partnersPriceWV = $partnersPricesValues['priceWV'];

        $responseMessage = '';
        if (isset($_GET['response']) && $_GET['response'] != '') {
            switch ($_GET['response']) {
                case 'success':

                    if (isset($_GET['discountApplied']) && $_GET['discountApplied'] == 'yes'){
                        if (isset($_GET['formDiscount']) && $_GET['formDiscount'] == 'BANKS750'){
                            $price = $banksPrice;
                            $vat = $banksVat;
                            $priceWV = $banksPriceWV;
                        }
                        elseif (isset($_GET['formDiscount']) && $_GET['formDiscount'] == 'MD1625%'){
                            $price = $partnersPrice;
                            $vat = $partnersVat;
                            $priceWV = $partnersPriceWV;
                        }
                    }

                    $responseMessage .= "<span style='color:#0A6AA2;'>Payment has been successfully sent.<br/> An email has been sent to {$customerEmail} with the receipt</span>";
                    $emailTemplateHeader = "Many thanks for your registration to Mobey Day.\n\nYour payment has been processed, a receipt will be sent to you within a few days.\n\n";
                    $emailTemplateHeader .= "1 Pass to Mobey Day\n\n";
                    $emailTemplateFooter = "If you have any questions, please contact us\n\n";
                    $emailTemplateFooter .= "Laura Sanguinetti\n";
                    $emailTemplateFooter .= "laura.sanguinetti@mobeyforum.org\nTel +358 40 355 1983";
                    $emailTemplateBody = "{$emailTemplateHeader}Price without VAT\n";
                    $emailTemplateBody .= "€ ".number_format($priceWV,2)."\n";
                    $emailTemplateBody .= "Price including VAT (".$vatPercent."%)\n";
                    $emailTemplateBody .= "€ ".number_format($price,2)."\n";
                    $emailTemplateBody .= "VAT paid\n";
                    $emailTemplateBody .= "€ ".number_format($vat,2)."\n\n\n{$emailTemplateFooter}";
                    $emailContent = $emailTemplateBody;
                    if ($customerEmail !== null) {
                        $headers = "From: Mobey Forum <mobeyforum@mobeyforum.org>\r\n";
                        wp_mail($customerEmail, "Receipt for Mobey Day Payment", $emailContent, $headers);
                    }
                    break;
                case 'cancel':
                    $responseMessage .="<span style='color:red'>Payment cancelled!</span>";
                    break;
                default:
                    break;
            }
        }
        ob_start();
        ?>

        <script type="text/javascript">
            var pageUrl = '<?php echo home_url('/mobey-day-payment'); ?>';
            var responsePageUrl = pageUrl + "?response";
            var costValue = parseFloat('<?php echo str_replace(",", ".", $price); ?>');
            var discountApplied = false;
            var formDiscount = '';
            function SetFields() {
                var customerFirstName = jQuery('#Customer_First_Name').val();
                var customerLastName = jQuery('#Customer_Last_Name').val();
                var orderID = jQuery('#Order_ID').val();
                var amountForm = jQuery('#AmountForm').val();
                var companyName = ', Company Name: ' + jQuery('#Company_Name').val();

                //Validate
                if (jQuery.trim(customerFirstName) === '' || jQuery.trim(customerLastName) === '' ||
                        jQuery.trim(orderID) === '' || jQuery.trim(amountForm) === '') {
                    jQuery('#message-warning-item').html('All fields are required!');
                    jQuery('#message-warning-box').css('display', 'block');
                    return false;
                }

                var amount = amountForm.replace(",", ".");
                amount = parseFloat(amount);
                if (isNaN(amount)) {
                    jQuery('#message-warning-item').html('Invalid Amount!');
                    jQuery('#message-warning-box').css('display', 'block');
                    return false;
                }

                amount = amount.toFixed(2);
                jQuery('#Amount').val(amount);

                jQuery('#message-warning-box').css('display', 'none');



                //Populate fields
                var orderDescription = encodeURI('Customer:' + customerFirstName + ' ' + customerLastName + companyName + ', Reference number:' + orderID + ', Amount:' + amountForm);
                orderDescription = orderDescription.replace(/'/g, "%27");
                jQuery('#Order_Description').val(orderDescription);

                var email = jQuery('#Customer_Email').val();
                var responseParameters = encodeURI('&ordernr=' + orderID + '&firstname=' + customerFirstName + '&lastname=' + customerLastName + '&companyname=' + jQuery('#Company_Name').val() + '&email=' + email);
                responseParameters = responseParameters.replace(/'/g, "%27");
                if ( discountApplied ) {
                    responseParameters += "&discountApplied=yes&formDiscount="+encodeURIComponent(formDiscount);
                }
                jQuery('#Success_Url').val(responsePageUrl + '=success' + responseParameters);
                jQuery('#Cancel_Url').val(responsePageUrl + '=cancel' + responseParameters);

                var data = {
                    action: 'pt_generate_auth',
                    amount: amount,
                    orderNr: orderID,
                    returnAddress: jQuery('#Success_Url').val(),
                    cancelAddress: jQuery('#Cancel_Url').val(),
                    orderDescription: jQuery('#Order_Description').val()
                };

                jQuery.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    data: data,
                    success: function (result) {
                        jQuery('#paytrail-auth-code').val(result);
                        jQuery('#ptform').submit();
                    }
                });
            }
            jQuery(document).ready(function ($) {
    <?php
    if ($_GET['response'] == "success") {
        echo "$('input[name=\"SendPaymentButton\"]').attr('disabled', 'disabled');\n";
        echo "$('#hideFieldsDiv').css('display', 'none');\n";
    }
    ?>
                $("#discountCode").change(function () {
                    if ($(this).val() === "MD1625%") {
                        discountApplied = true;
                        formDiscount = $(this).val();
                        var costWithDiscount = "<?php echo number_format($partnersPrice,2,'.',''); ?>";
                        $("#AmountForm").val(costWithDiscount);
                    }
                    else if ($(this).val() === "BANKS750") {
                        discountApplied = true;
                        formDiscount = $(this).val();
                        var costWithDiscount = "<?php echo number_format($banksPrice,2,'.',''); ?>";
                        $("#AmountForm").val(costWithDiscount);
                    }
                    else {
                        discountApplied = false;
                        formDiscount = '';
                        $("#AmountForm").val(costValue);
                    }
                });
            });
           </script>
        <?php
        if ($discount > 0){
            ?>
            <p><strong style="color: #000000;">Standard Ticket Price is <?php echo number_format($customerDefaultCost,2,'.',''); ?>€.</strong></p>
            <?php
            if ( $now >= $d1Start && $now <= $d1End){
                $untilDate = date('jS F',$d1End);
            }
            elseif ( $now >= $d2Start && $now <= $d2End){
                $untilDate = date('jS F',$d2End);
            }
            ?>
            <p><strong style="color: #000000;"><?php echo $discount; ?>% Discount </strong><span style="color: #000000;">valid until <?php echo $untilDate; ?>.</span></p>
            <?php
        }
        ?>
        <form id="ptform" action="https://payment.paytrail.com/" methos="post">
            <div class="validation_error" id="message-warning-box" style="display:none;" >
                <ul>
                    <li id="message-warning-item"></li>
                </ul>
            </div>
            <?php if (isset($responseMessage) && $responseMessage != '') {
                ?>
                <div class="validation_error">
                    <?php echo $responseMessage; ?>
                </div>
            <?php } ?>

            <div id="hideFieldsDiv">
                <b>Reference number:</b>
                <div>
                    <input readonly="readonly" type="text" name="ORDER_NUMBER" id="Order_ID" value="<?php echo $customerOrderNr; ?>" />
                </div>
                <b>Discount Code</b>
                <div>
                    <input type="text" id="discountCode" value="" />
                </div>
                <b>Amount</b>
                <div>
                    <input readonly="readonly" type="text" id="AmountForm" value="<?php echo number_format($price,2,'.',''); ?>" />
                    <input type="hidden" name="AMOUNT" id="Amount" value="" /> <?php echo get_option('pt_currency_symbol'); ?>
                </div>

                <b>First Name</b>
                <div>
                    <input type="text" id="Customer_First_Name" value="<?php echo $customerFirstName; ?>"  />
                </div>

                <b>Last Name</b>
                <div>
                    <input type="text" id="Customer_Last_Name" value="<?php echo $customerLastName; ?>"  />
                </div>

                <b>Email</b>
                <div>
                    <input type="text" id="Customer_Email" value="<?php echo $customerEmail; ?>"  />
                </div>

                <b>Company Name</b>
                <div>
                    <input type="text" id="Company_Name" value="<?php echo $customerCompanyName; ?>"  />
                </div>

                <div>
                    <input type="hidden" name="AUTHCODE" value="<?php echo get_option('pt_merchant_auth'); ?>" />
                    <input type="hidden" name="MERCHANT_ID" value="<?php echo get_option('pt_merchant_id'); ?>" />
                    <!-- <input name="REFERENCE_NUMBER" type="hidden" value=""> -->
                    <input type="hidden" name="ORDER_DESCRIPTION" value="" id="Order_Description">
                    <input type="hidden" name="CURRENCY" value="<?php echo get_option('pt_currency_code'); ?>" /> <!-- Only EUR is accepted for Finnish banks and credit cards. -->
                    <input type="hidden" name="RETURN_ADDRESS" id="Success_Url" value="" />
                    <input type="hidden" name="CANCEL_ADDRESS" id="Cancel_Url" value="" />
                    <!-- <input name="PENDING_ADDRESS" type="hidden" value=""> -->
                    <!-- <input name="NOTIFY_ADDRESS" type="hidden" value=""> -->
                    <input type="hidden" name="TYPE" value="S1" />
                    <input type="hidden" name="CULTURE" value="en_US">
                    <!-- <input name="PRESELECTED_METHOD" type="hidden" value=""> -->
                    <!-- <input name="MODE" type="hidden" value="1"> -->
                    <!-- <input name="VISIBLE_METHODS" type="hidden" value=""> -->
                    <!-- <input name="GROUP" type="hidden" value=""> -->
                    <input type="hidden" name="AUTHCODE" id="paytrail-auth-code" value="">
                    <p>
                    <input type="button" class="button" onclick="SetFields();" name="SendPaymentButton" value="Send" />
                    </p>
                </div>  
                <br/>
                <div class="payment-service-info">
                    <p>Payment service provider</p>
                    <br/>
                    <p>Paytrail Oyj (2122839-7) acts as an implementer of the payment handling service and as a Payment Service Provider. Paytrail Oyj will be shown as the recipient in the invoice and Paytrail Oyj will forward the payment to the merchant.</p>
                    <p>Paytrail Oyj is an authorized Payment Institution. For reclamations, please contact the website you made your payment to.</p>
                    <br/>
                    <p>Paytrail Oyj, business ID 2122839-7</p>
                    <p>Innova 2</p>
                    <p>Lutakonaukio 7<p>
                    <p>40100 Jyväskylä</p>
                    <p>Phone: +358 207 181830</p>
                    <br/>
                    <p>Netbanks</p>
                    <br/>
                    <p>Paytrail Oyj (FI2122839) provides netbank related payment transfer services in co-operation with Finnish banks and credit institutions. For consumer the service works exactly the same way as traditional web payments.</p>
                </div>
            </div>
        </form>

        <?php
        return ob_get_clean();
    }

    add_shortcode('mobey-day-payment', 'mobey_day_payment_shortcode');

    /**
     * Gravity Forms File Upload
     */
    add_action('admin_footer', 'mf_add_member_on_button');

    function mf_add_member_on_button() {
        ?>
        <script type="text/javascript" >
            jQuery(document).ready(function ($) {

                // WSX
                $('#accept_entry_button').click(function () {
                    jQuery('#please_wait_container_ad').fadeIn();
                    var data = {
                        action: 'mf_accept_entry',
                        lead: $('#accept_entry_button').attr('data-id'),
                        email: $('#accept_entry_button').attr('data-email')
                    };
                    $.post(ajaxurl, data, function (response) {
                        alert(response);
                        jQuery('#please_wait_container_ad').hide();
                        window.location.href = window.location.href
                    });
                });
                $('#reject_entry_button').click(function () {
                    jQuery('#please_wait_container_ad').fadeIn();
                    var data = {
                        action: 'mf_reject_entry',
                        lead: $('#reject_entry_button').attr('data-id'),
                        email: $('#reject_entry_button').attr('data-email')
                    };
                    $.post(ajaxurl, data, function (response) {
                        alert(response);
                        jQuery('#please_wait_container_ad').hide();
                        window.location.href = window.location.href
                    });
                });
                // END

            });
        </script><?php
    }

// WSX

    add_action('wp_ajax_mf_accept_entry', 'mf_accept_entry_callback');

    function mf_accept_entry_callback() {
        global $wpdb;

        $lead_details_table = GFFormsModel::get_lead_details_table_name();

        $user_email = $_POST['email'];
        $user = get_user_by('email', $user_email);
        Groups_User_Group::create(array("user_id" => $user->id, "group_id" => 2));

        $wpdb->query("UPDATE $lead_details_table SET `value`= 'Accepted' WHERE form_id=1 AND lead_id={$_POST['lead']} AND field_number=7");

        $form = GFFormsModel::get_form_meta_by_id(1);
        $notification = $form[0]["notifications"]["54fe0b9746f6e"];
        $notification["to"] = $user_email;
        $notification["toType"] = "email";
        $lead = RGFormsModel::get_lead($_POST['lead']);
        GFCommon::send_notification($notification, $form, $lead);

        echo 'User added to Members.';
        die();
    }

    add_action('wp_ajax_mf_reject_entry', 'mf_reject_entry_callback');

    function mf_reject_entry_callback() {
        global $wpdb;

        $lead_details_table = GFFormsModel::get_lead_details_table_name();

        $user_email = $_POST['email'];
        $user = get_user_by('email', $user_email);
        Groups_User_Group::delete($user->id, 2);

        $wpdb->query("UPDATE $lead_details_table SET `value`= 'Rejected' WHERE form_id=1 AND lead_id={$_POST['lead']} AND field_number=7");

        $form = GFFormsModel::get_form_meta_by_id(1);
        $notification = $form[0]["notifications"]["55fbdac6778d5"];
        $notification["to"] = $user_email;
        $notification["toType"] = "email";
        $lead = RGFormsModel::get_lead($_POST['lead']);
        GFCommon::send_notification($notification, $form, $lead);

        echo 'User rejected!';
        die();
    }

    add_action("gform_entry_detail_sidebar_before", "mf_entry_detail_accept_reject_box", 10, 2);

    function mf_entry_detail_accept_reject_box($form, $lead) {

        if ($form["id"] == 1) {
            echo "<div class='stuffbox'><h3>ACCEPT / REJECT ENTRY</h3>
               <div class='inside' style='padding:20px;' >
                   <input id='accept_entry_button' type='button' class='button' data-id='" . $lead["id"] . "' data-email='" . $lead["1"] . "' value='Accept'>
                   <input id='reject_entry_button' type='button' class='button' data-id='" . $lead["id"] . "' data-email='" . $lead["1"] . "' value='Reject' style='margin-left:40px;'>
                   <span id='please_wait_container_ad' style='display:none; margin-left: 5px;'><img src='" . GFCommon::get_base_url() . "/images/spinner.gif' /> Resending...</span>
               </div>
        </div>";
        }
    }

    // Auto-accepting from the list of emails

    add_action('gform_after_submission', 'auto_accept', 10, 2);

    function auto_accept($entry, $form) {

        global $wpdb;
        $GLOBALS['acceptedReg'];

        $lead_details_table = GFFormsModel::get_lead_details_table_name();

        if ($form["id"] == 1) {
            $emails = file_get_contents(ABSPATH . "download/list_of_emails.csv");
            $user_email = rgar($entry, '1');
            $split = explode("@", $user_email);
            $lead_id = rgar($entry, 'id');
            $find = strpos($emails, $split[1]);
            if ($find == FALSE) {

                $wpdb->query("UPDATE $lead_details_table SET `value`= 'Rejected' WHERE form_id=1 AND lead_id = $lead_id AND field_number=7");

                $form = GFFormsModel::get_form_meta_by_id(1);
                $notification = $form[0]["notifications"]["55fbdac6778d5"];
                $notification["to"] = $user_email;
                $notification["toType"] = "email";
                $lead = RGFormsModel::get_lead($lead_id);
                GFCommon::send_notification($notification, $form, $lead);
                $GLOBALS['acceptedReg'] = 0;

            } else {

                $wpdb->query("UPDATE $lead_details_table SET `value`= 'Accepted' WHERE form_id=1 AND lead_id = $lead_id AND field_number=7");

                $form = GFFormsModel::get_form_meta_by_id(1);
                $notification = $form[0]["notifications"]["54fe0b9746f6e"];
                $notification["to"] = $user_email;
                $notification["toType"] = "email";
                $lead = RGFormsModel::get_lead($lead_id);
                GFCommon::send_notification($notification, $form, $lead);
                $GLOBALS['acceptedReg'] = 1;
            }
        }
    }

//END
?>
