<?php get_header(); ?>


		<!-- section -->
<section class="basic-header white-text">
   <div class="wrapper">
      <h1 class="center">Event registration</h1>

       <div class="clear"></div>
   </div>
</section>

<!-- section -->
<section class="basicpage-main-content event-register-main">
   <div class="wrapper">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php
	if ( has_post_thumbnail() ) {
          print "<div class=\"blog-thumb\">";
          the_post_thumbnail('large');
           print "</div>";
          } 		
?>          
<h1><?php the_title(); ?></h1>

<?php    
global $wp;

$firstname = $_POST["firstname"];
$lastname = $_POST["lastname"];
$employer = $_POST["employer"];
$press_input = $_POST["press_input"];


  $custom = get_post_custom($post->ID);
$eventbrite_id= $custom["eventbrite_id"][0]; 
$gravity_forms= $custom["gravity_forms"][0];

    if ( is_user_logged_in() ) {
    print "<h3>Mobey Member Registration</h3>";
    if ($gravity_forms)print do_shortcode('[gravityform id='.$gravity_forms.' title=false description=false ajax=true]');
    } else {
    $current_url = home_url(add_query_arg(array(),$wp->request));
      print "<h3>Mobey Member?</h3>";
      print "<p>Mobey Members can sign up for free, but first you need to sign in.</p>";
      echo "<a href=\"#\" class=\"login-button button secondary big-login-button\">Sign in</a>";
     }
      if($eventbrite_id){
      print "<p>Non members may purchase a ticket.</p>";
      print "<p><iframe id=\"eventbrite-tickets-$eventbrite_id\" src=\"//www.eventbrite.com/tickets-external?eid=$eventbrite_id\" style=\"max-width: 500px;width:100%;height:600px; border: 0px;\"></iframe></p>";
			}
    
     //print "<p>Press & media can register  members may purchase a ticket.</p>";
  ?>
  <!--
  <form method="post" action="?press=1">
  First name:<br>
  <input type="text" required name="firstname" value="<?php echo $firstname; ?>"><br>
  Last name:<br>
  <input type="text" required name="lastname" value="<?php echo $lastname; ?>"><br><br>
  Employer:<br>
  <input type="text" required name="employer" value="<?php echo $employer; ?>"><br><br>
  Code:<br>
  <input type="text" required name="press_input" value="<?php echo $press_input; ?>"><br><br>
  <input type="submit" value="Submit">
</form>-->
  <?php
  ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>
<br class="clear">
			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>
    </div>
		</section>
		<!-- /section -->




<?php get_footer(); ?>
