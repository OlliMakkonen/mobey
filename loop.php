<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
<article class="loop-item">
				 <?php
				 $category = get_the_category();
				 $cate = $category[0]->term_id;
         if ($cate ==0){ 
         
                  print "<div class=\"vw-container\"><div class='video-thumbnail'>";
         $i++; 
         $custom = get_post_custom($post->ID);
          $youtube_url= $custom["youtube-url"][0];
         print "<a class=\"read-more\" target=\"_blank\" href=\"$youtube_url\"><div class=\"play\"></div>";
         
          if ( has_post_thumbnail() ) {
         the_post_thumbnail( 'medium' );
} 
          ?></a></div><div class="content">
        <div class="video-indicator">Video</div>
        <div class="heading-div-db"><span class="news-heading"><?php the_title(); ?></span></div>
        <div class="blogs-date"><?php print get_the_date('jS \o\f F Y'); ?>
        <div class="blog-link"><a class="read-more" target="_blank" href="<?php print $youtube_url; ?>">Watch now</a></div>
        </div>
        
      </div>
      
   </div>
   <?php
         
          
          }
          else{
         print "<div class=\"search-result-container\">";
         $i++; 
         
         if (str_word_count (get_the_title())<5)$long_excerpt=true; else $long_excerpt=false;
         if (str_word_count (get_the_title())>10)$no_content=true; else $no_content=false;
         
         
          if ( has_post_thumbnail() ) {
         print "<div class='reports-thumbnail'>";
         the_post_thumbnail( 'reports' );
         print "</div>";
} 
          ?>
          <div class="blog-content-right"> 
        <?php if ($cate ==4) print '<div class="blog-indicator">Blog</div>'; 
        if ($cate ==98) print '<div class="report-indicator">Report</div>';
        if ($cate ==7) print '<div class="press-indicator">Press release</div>';
        if ($cate ==149) print '<div class="webinar-indicator">Webinar</div>';
        
         ?>
         
        <div class=" blog-heading-div"><a class="news-heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
        <?php if ($long_excerpt==true) { ?> <div class="blogs-content"><?php html5wp_excerpt(30); ?></div> <?php } ?>

        <?php if ($long_excerpt==false && $no_content==false) { ?> <div class="blogs-content"><?php html5wp_excerpt(20); ?></div> <?php } ?>
        <div class="blogs-date"><?php print get_the_date('jS \o\f F Y'); ?>
              <div class="blog-link"><a class="read-more" href="<?php the_permalink(); ?>">Read more </a></div>
        </div>
      </div>
      <div class="clear"></div>
   </div>
   <div class="clear"></div>
   <?php } ?>
   
   </article>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<p><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></p>
	</article>
	<!-- /article -->

<?php endif; ?>
