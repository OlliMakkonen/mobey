<?php  /* Template Name: Workgroups Page Template */ 

//if (!is_user_logged_in() ) wp_redirect( site_url( 'home' ) );
get_header(); 
 $current_user = wp_get_current_user();
$mobey_workgroups =  get_option('mobey_workgroups'); 

?>

<!-- section -->

<section class="basic-header white-text workgroups-header">
   <div class="wrapper">
      <h1 class="center">Workgroups</h1>

       <div class="clear"></div>
   </div>
</section>
<section class="basicpage-main-content workgroups-main">
   <div class="wrapper center">
   
         <?php if (have_posts()): while (have_posts()) : the_post(); ?>
      <!-- article -->
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
         <?php the_content(); ?>
         <br class="clear">
      </article>
      <!-- /article -->
      <?php endwhile; ?>
      <?php endif; ?>
    </div>
          
</section>
<section class="workgroups-main-content-bottom">
  <div class="wrapper">
   
    <div class="wk1">
    <img src="<?php echo get_template_directory_uri(); ?>/img/bubbels@2x.png" alt="Mobey Forum" class="s35 center">
<h4>Strategic discussion</h4>
Talk among experts leads to new & intersting ideas.
    </div>
    <div class="wk-triangle1">
    </div>
     <div class="wk2">
     <img src="<?php echo get_template_directory_uri(); ?>/img/handshake@2x.png" alt="Mobey Forum" class="wimg2 s35 center">
    
 <h4>Collaboration</h4>
   Our online and one-site gatherings pave ways to fruitful co-operation.
    </div>
    <div class="wk-triangle2">
    </div>
       <div class="wk3">
    <img src="<?php echo get_template_directory_uri(); ?>/img/text-line-form-copy@2x.png" alt="Mobey Forum" class="wimg3 s35 center">

    <h4>Content</h4>
    Workgroups produce reports, infographics, videos & webinars 

    </div>
    
  </div>          
  <div class="clear"></div>
</section>
<section class="basicpage-main-content workgroups-main">
   <div class="wrapper center">
   <p class="size20">
The groups meet online on regular basis and in face-to-face meetings at the workgroup days before Mobey member meetings. Mobey Forum’s reports are produced by the workgroups.
 </p>
  </div>          
  <div class="clear"></div>
</section>
<section class="workgroups-current-workgroups">
  <div class="wrapper">
      <div class="wk-topwrapper">
  <h2>Current Workgroups</h2>
    <p class="size20">
      Mobey Forum is currently hosting four different workgroups. You can find more information on them below by clicking the arrow icon.
    </p> 
   </div>
    <div class="wk-column1">
        <div class="wk-box wk-box1">
        <h3><?php print $mobey_workgroups['box1_heading']; ?></h3>
          <div class="long">
       <?php print apply_filters('the_content', $mobey_workgroups['currentbox_text']);  ?>

        </div>
        <div class="workgroups-arrow"></div>
        <?php if (is_user_logged_in())print '<div><span class="open-workgroups-modal button primary">Join Workgroup</span></div>'; ?>      </div>
       
        <div class="wk-box wk-box2">
        <h3><?php print $mobey_workgroups['box2_heading']; ?></h3>
        <div class="long">
        <?php print apply_filters('the_content', $mobey_workgroups['currentbox2_text']);  ?>      </div>
        <div class="workgroups-arrow"></div>
        <?php if (is_user_logged_in())print '<div><span class="open-workgroups-modal button primary">Join Workgroup</span></div>'; ?>      </div>
    </div>  
    <div class="wk-column2"> 
      <div class="wk-box wk-box3">
      <h3><?php print $mobey_workgroups['box3_heading']; ?></h3>
      <div class="long">

      <?php print apply_filters('the_content', $mobey_workgroups['currentbox3_text']);  ?>
      </div>
      <div class="workgroups-arrow"></div>
      <?php if (is_user_logged_in())print '<div><span class="open-workgroups-modal button primary">Join Workgroup</span></div>'; ?>      </div>
      <div class="wk-box wk-box4">
      <h3><?php print $mobey_workgroups['box4_heading']; ?></h3>
      <div class="long">
      <?php print apply_filters('the_content', $mobey_workgroups['currentbox4_text']);  ?>      </div>
      <div class="workgroups-arrow"></div>
      <?php if (is_user_logged_in())print '<div><span class="open-workgroups-modal button primary">Join Workgroup</span></div>'; ?>      </div>
    </div>
      
     <div class="clear"></div>
  </div>
</section>
<section class="workgroups-access">
  <div class="wrapper">
<h1>Get access to our Workgroups</h1>
<p class="size20">
Only <b>member companies’</b> employees can join our workgroups.
Take a look at our <b>member benefits</b> & become a member today!</p>

  <div class="clear"><a class="button primary" href="/join/">Join</a></div>
  
  </div>
</section>

<!-- Modal -->
<div id="workgroupsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title">Join a Workgroup</h2>
      </div>
      <div class="modal-body">
         <?php print do_shortcode('[gravityform id=11 title=false description=false ajax=true]'); ?>
      </div>
     </div>

  </div>
</div>
  
<?php get_footer(); ?>

