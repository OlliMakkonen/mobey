jQuery(document).ready(function($) {

  // Hide Header on on scroll down
  var didScroll;
  var lastScrollTop = 0;
  var delta = 5;
  var navbarHeight = 200;

      var clone = $(".helpcontainer").clone(true).appendTo('body').addClass("fixedTop-main");
      var clonesearch = $(".copycontainer").clone(true).appendTo('.fixedTop-main').addClass("fixedTopSearch");
clone.hide();

  $(window).scroll(function(event){
   if (window.matchMedia('(max-width: 1080px)').matches){clone.hide();
   }else didScroll = true;
  });

  setInterval(function() {
      if (didScroll) {
          hasScrolled();
          didScroll = false;
      }
  }, 250);

  function hasScrolled() {
      var st = $(this).scrollTop();
      // console.log(st + " > " + navbarHeight);
      // Make sure they scroll more than delta
      if(Math.abs(lastScrollTop - st) <= delta) return;
      
      if (st>150) clone.fadeIn();     
       else clone.fadeOut();
             
            
       
      
      lastScrollTop = st;
  }
});