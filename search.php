<?php get_header(); ?>
<!-- section -->

<section class="basic-header white-text">
   <div class="wrapper center"><br>
 <h1 class="center">Search Results</h1>

   </div>
</section>
<?php 
        $category = get_the_category();
         wp_reset_postdata(); 
?>        
<section class="search-row">
    <div class="wrapper">
<form class="search " method="get" action="<?php echo home_url(); ?>" role="search">
  <input class="search-input1" type="search" name="s" placeholder="<?php _e( 'Search', 'html5blank' ); ?>">
  <button class="search-submit button primary" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
</form>
   </div>
</section>

<section class="search-results-main">
   <div class="wrapper">


			<p class="size20 center"><?php echo sprintf( __( 'We found %s Search Results for "', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?>"</p>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

 <div class="clear"></div>
   </div>
</section>   
<?php get_footer(); ?>
