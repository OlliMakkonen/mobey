<?php  /* Template Name: Events Page Template */ 

//if (!is_user_logged_in() ) wp_redirect( site_url( 'home' ) );
get_header(); 
 $current_user = wp_get_current_user();
$mobey_events =  get_option('mobey_events'); 

?>

<!-- section -->

<section class="basic-header white-text">
   <div class="wrapper">
      <h1 class="center">Events</h1>

       <div class="clear"></div>
   </div>
</section>
<section class="basicpage-main-content events-main">
   <div class="wrapper center">
   
         <?php if (have_posts()): while (have_posts()) : the_post(); ?>
      <!-- article -->
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
         <?php the_content(); ?>
         <br class="clear">
      </article>
      <!-- /article -->
      <?php endwhile; ?>
      <?php endif; ?>
   

    </div>
          
</section>
<section class="events-main-content-bottom">
  <div class="wrapper">
  <div class="center">
    <span class="size26 light">We host <b>two types</b> of Mobey Forum events</span><br><br>
    <div class="events-main-content-box">
       <div class="emcb-left">
            <h3 class="aka-mobey-day">Open events</h3>        
            MobeyDay is an event that brings together the financial services and fintech industries globally for two days of learning, analyzing and collaborating.
        
       </div>  
              <div class="emcb-right-button"><a class="button primary" href="http://mobeyday.com/">next open event</a></div>



       <div class="emcb-right">
            <h3>Member Events</h3>
            Member events are only for Mobey registered members. We meet as peers with a mutual interest in innovating and developing financial services. Our shared passion leads to a fruitful exchange of ideas, and everyone ends up with much broader insight.
       </div>
              <div class="emcb-left-button"><a class="button primary" href="#member">next member event</a></div>       
         
    </div>
    
     <?php 
   
               $active_printed=false;
               $indicators = array();
               wp_reset_postdata();  
               $args = array(
               'post_type' => 'Events',
               'posts_per_page' => 4, 
                'orderby' => 'menu_order', 
                 'order' => 'ASC', 
               );
               $i=0;
               $posts_array = get_posts( $args ); 
               foreach ( $posts_array as $post ) : setup_postdata( $post ); 
               
             //  $post = $posts_array[$i];
               $permalink = get_permalink($post);
               $custom = get_post_custom($post->ID);
               $location= $custom["location"][0]; 
               $members_only= $custom["members_only"][0]; 
               $past_event= $custom["past_event"][0]; 
               $theme= $custom["theme"][0];
               $headingi= $custom["heading"][0];
               $start_date= $custom["alkaa_pv"][0];
               $end_date= $custom["loppuu_pv"][0];
               $year = substr($alkaa_pv, -4);
               $short_info_text= $custom["short_info_text"][0];
               if ($past_event==0 &&  $active_printed==false){$next=true; $active = " active"; $active_printed=true;} else $active = "";

                       
          $fully_booked = $custom["booked"][0];
$indeksi = strtotime($start_date." 15:00:$i");
$indeksi2 = strtotime($end_date." 15:00:$i");          

 
 $date1 = new DateTime();
$date1->setTimestamp($indeksi);

$date2 = new DateTime();
$date2->setTimestamp($indeksi2);

if ($date1->format('Y-m') === $date2->format('Y-m')) {
$reader_friendly = date('jS', $indeksi)." - ".date('jS \o\f F, Y', $indeksi2);
}else {
$reader_friendly = date('jS \o\f M', $indeksi)." - ".date('JS \o\f M, Y', $indeksi2);
} 
               
               
               
               $thumbnail_url = get_the_post_thumbnail_url($post->ID, 'large');
               $thumbnail_url2 = get_the_post_thumbnail_url($post->ID, 'medium');
               
               
             
               $sid=$i+1;
               //create the indicator
               $indicators[$i] =   "<div class=\"events-cell events-cell-link $active\" style=\"background-image: url($thumbnail_url2);\">";
               if ($active!=="") $indicators[$i] .= '<div class="up-next">Up Next</div>';
               $indicators[$i] .= '<div class="passive-border"></div><div class="passive-indicator"><p>';
               $indicators[$i] .= $location;
               $indicators[$i] .=  "<br><span class=\"size26 bold\">$headingi</span></p>";
               $indicators[$i] .= "<div class=\"size14 bold\">$reader_friendly</div></div></div>";
                       $i++;
                     ?>      
            <?php endforeach; 
               wp_reset_postdata();?>
    
    <div class="events-row">
            <?php
               foreach ($indicators as $indicator) {
                  echo $indicator;
               }
                ?>
               
         </div>
       <div class="clear"></div>
      </div>
    
  </div>
</section>

<section class="events-open-events">
  <div class="wrapper">
    <h2 class="center">Open Events</h2>
      <p class="main-text">
            MobeyDay is an event that brings together the financial services and fintech industries globally for two days of learning, analyzing and collaborating.
      </p>
      
      <?php 
    
               wp_reset_postdata();  
               $args = array(
               'post_type' => 'Events',
               'posts_per_page' => 4, 
                'orderby' => 'page_order', 
                 'order' => 'ASC', 
               );
               $i=0;
               $posts_array = get_posts( $args ); 
               foreach ( $posts_array as $post ) : setup_postdata( $post ); 
               
             //  $post = $posts_array[$i];
               $permalink = get_permalink($post);
               $custom = get_post_custom($post->ID);
               $location= $custom["location"][0]; 
               $members_only= $custom["members_only"][0]; 
               $past_event= $custom["past_event"][0];
               $theme= $custom["theme"][0];
               $headingi= $custom["heading"][0];
               $start_date= $custom["alkaa_pv"][0];
               $end_date= $custom["loppuu_pv"][0];
               $year = substr($alkaa_pv, -4);
               $short_info_text= $custom["short_info_text"][0];
               $button_1_text = $custom['pb-text'][0];
               $button_1_link = $custom['pb-url'][0];
               $button_2_text = $custom['sb-text'][0];
               $button_2_link = $custom['sb-url'][0];
               
                if ($i==0)$active = " active"; else $active = "";
                                     
                        $fully_booked = $custom["booked"][0];
              $indeksi = strtotime($start_date." 15:00:$i");
              $indeksi2 = strtotime($end_date." 15:00:$i");          

               
               $date1 = new DateTime();
              $date1->setTimestamp($indeksi);

              $date2 = new DateTime();
              $date2->setTimestamp($indeksi2);

              if ($date1->format('Y-m') === $date2->format('Y-m')) {
              $reader_friendly = date('jS', $indeksi)." - ".date('jS \o\f F, Y', $indeksi2);
              }else {
              $reader_friendly = date('jS \o\f M', $indeksi)." - ".date('JS \o\f M, Y', $indeksi2);
              } 
                             
               
               
               $thumbnail_url = get_the_post_thumbnail_url($post->ID, 'large');
               $thumbnail_url2 = get_the_post_thumbnail_url($post->ID, 'medium');
               
               if ( $members_only != 1 && $past_event!=1 && $one_done==true){
               
                print "<div class=\"small-event-container\">";    
            
            print "<div class=\"small-event-image\" style=\"background-image: url($thumbnail_url);\">";
            print "<div class=\"small-event-content\">";
            print "coming later this year";
            print "<h2 class=\"small-event-heading\">Mobey Day 2#: <span class=\"light\">$location</span></h2>\n";
             print "<div class=\"small-event-meta\">";
            if ($theme)print "<div class=\"featured-theme\"><div class=\"theme\"></div> $theme</div>";
            print "<div class=\"featured-date\"><div class=\"calendar\"></div> $reader_friendly</div>";
            print "</div></div>";
             print "<div class=\"small-event-triangle\"></div>";
            print "<div class=\"small-event-buttons\">";
            
            
            print '<span class="addtocalendar atc-style-blue">';
            print '<var class="atc_event">';
            print '<var class="atc_date_start">'.$start_date.'</var>';
            print '<var class="atc_date_end">'.$end_date.'</var>';
            print '<var class="atc_timezone">Europe/London</var>';
            print '<var class="atc_title">'.$headingi.'</var>';
            print '<var class="atc_description">'.$short_info_text.'</var>';
            print '<var class="atc_location">'.$location.'</var>';
            print '<var class="atc_organizer">Mobey Forum</var>';
            print '<var class="atc_organizer_email">mobeyforum@mobeyforum.org</var>';
            print '</var>';
            print '</span>';
            
            //print '<a class="button primary inline" href="#">Add to google cal.</a>';
            //print '<a class="button primary inline" href="#">Add to calendar</a> ';
            print "</div><div class=\"clear\"></div></div></div>\n";   
                                
                break;
                }
               
               
               
               if ($members_only != 1 && $past_event!=1 && $i==0){
               $one_done=true;
               //create slide in carousel  
              print "<div class=\"\">\n";
;
               print "<div class=\"featured-event-container\">";
               print "<div class=\"featured-event-image\" style=\"background-image: url($thumbnail_url);\">";
               print '<img src="'.get_template_directory_uri().'/img/event-image-mask.png" alt="mobey" class="event-image-mask">';
               print "</div>";
               print "<div class=\"featured-event-content\">";   
               print "<div class=\"featured-event-heading-no-diamond\">\n";
               print "<h2 class=\"featured-heading\">$location</h2>\n";
               print "<h3 class=\"featured-sub-heading\">$headingi</h3>\n";
               if ($members_only == 1)print "<div class=\"members-only-diamond\">Members<br> only</div>";

               print "</div>";  
               print "<div class=\"featured-event-information\">";
               if ($theme)print "<div class=\"featured-theme\"><div class=\"calendar\"></div> $theme</div><br>\n";
               print "<div class=\"featured-date\"><div class=\"theme\"></div> $reader_friendly</div>";
               print "<div class=\"featured-short-info\">$short_info_text</div></div>\n";
               ?>
             <p class="event-buttons">
           <?php if ($button_1_link){ ?>
            <a class="button primary inline" href="<?php print $button_1_link; ?>"><?php print $button_1_text; ?></a>
            <?php
                  }
                  if ($button_2_link){
                  ?>
            <a class="button secondary inline" href="<?php print $button_2_link ; ?>"><?php print $button_2_text; ?></a> 
            
            <?php
               }
            
               print "</div></div></div>\n";       
               $sid=$i+1;
               $i++;
               
               }
                
                     ?>      
            <?php endforeach; 
               wp_reset_postdata();
    
           
            ?>
      <div class="clear"></div>      
  </div>
</section>

<section class="events-member-events">
  <div class="wrapper">
   <a name="member"></a> 
    <h2 class="center">Member Events</h2>
    <p class="main-text">
            Member events are only for Mobey registered members. We meet as peers with a mutual interest in innovating and developing financial services. Our shared passion leads to a fruitful exchange of ideas, and everyone ends up with much broader insight.
    </p>
    
     <?php 
    
               wp_reset_postdata();  
               $args = array(
               'post_type' => 'Events',
               'posts_per_page' => 3, 
                'orderby' => 'page_order', 
                 'order' => 'ASC', 
               );
               $i=0;
               $one_done=false;
               $posts_array = get_posts( $args ); 
               foreach ( $posts_array as $post ) : setup_postdata( $post ); 
               
             //  $post = $posts_array[$i];
               $permalink = get_permalink($post);
               $custom = get_post_custom($post->ID);
               $location= $custom["location"][0]; 
               $members_only= $custom["members_only"][0]; 
               $theme= $custom["theme"][0];
               $headingi= $custom["heading"][0];
               $start_date= $custom["alkaa_pv"][0];
               $end_date= $custom["loppuu_pv"][0];
               $year = substr($alkaa_pv, -4);
               $short_info_text= $custom["short_info_text"][0];
                if ($i==0)$active = " active"; else $active = "";
                                     
                        $fully_booked = $custom["booked"][0];
              $indeksi = strtotime($start_date." 15:00:$i");
              $indeksi2 = strtotime($end_date." 15:00:$i");          

               
               $date1 = new DateTime();
              $date1->setTimestamp($indeksi);

              $date2 = new DateTime();
              $date2->setTimestamp($indeksi2);

              if ($date1->format('Y-m') === $date2->format('Y-m')) {
              $reader_friendly = date('jS', $indeksi)." - ".date('jS \o\f F, Y', $indeksi2);
              }else {
              $reader_friendly = date('jS \o\f M', $indeksi)." - ".date('JS \o\f M, Y', $indeksi2);
              } 
                             
               
               
               $thumbnail_url = get_the_post_thumbnail_url($post->ID, 'large');
               $thumbnail_url2 = get_the_post_thumbnail_url($post->ID, 'medium');
               
                    if ( $members_only == 1 && $one_done==true){
               
                print "<div class=\"small-event-container\">";    
            
            print "<div class=\"small-event-image\" style=\"background-image: url($thumbnail_url);\">";
            print "<div class=\"small-event-content\">";
            print "coming later this year";
            print "<h2 class=\"small-event-heading\">Memmber event   2#: <span class=\"light\">$location</span></h2>\n";
            print "<div class=\"small-event-meta\">";
            if ($theme)print "<div class=\"featured-theme\"><div class=\"theme\"></div> $theme</div>";
            print "<div class=\"featured-date\"><div class=\"calendar\"></div> $reader_friendly</div>";
            print "</div></div>";
             print "<div class=\"small-event-triangle\"></div>";
            print "<div class=\"small-event-buttons\">";
            
             print '<span class="addtocalendar atc-style-blue">';
            print '<var class="atc_event">';
            print '<var class="atc_date_start">'.$start_date.'</var>';
            print '<var class="atc_date_end">'.$end_date.'</var>';
            print '<var class="atc_timezone">Europe/London</var>';
            print '<var class="atc_title">'.$headingi.'</var>';
            print '<var class="atc_description">'.$short_info_text.'</var>';
            print '<var class="atc_location">'.$location.'</var>';
            print '<var class="atc_organizer">Mobey Forum</var>';
            print '<var class="atc_organizer_email">mobeyforum@mobeyforum.org</var>';
            print '</var>';
            print '</span>';
            
            print "<div class=\"clear\"></div></div><div class=\"clear\"></div></div></div>\n";   
                                
                break;
                }
               
               
               if ($i==0 && $members_only == 1){
               //create slide in carousel  
               $one_done=true;
              print "<div class=\"\">\n";
;
               print "<div class=\"featured-event-container\">";
               print "<div class=\"featured-event-image\" style=\"background-image: url($thumbnail_url);\">";
               print '<img src="'.get_template_directory_uri().'/img/event-image-mask.png" alt="mobey" class="event-image-mask">';
               print "</div>";
               print "<div class=\"featured-event-content\">";   
               print "<div class=\"featured-event-heading\">\n";
               print "<h2 class=\"featured-heading\">$location</h2>\n";
               print "<h3 class=\"featured-sub-heading\">$headingi</h3>\n";
               
               print "</div>";  
               print "<div class=\"featured-event-information\">";
               if ($theme)print "<div class=\"featured-theme\"><div class=\"calendar\"></div> $theme</div><br>\n";
               print "<div class=\"featured-date\"><div class=\"theme\"></div> $reader_friendly</div>";
               print "<div class=\"featured-short-info\">$short_info_text</div></div>\n";
               ?>
            <p class="event-buttons"><a class="button secondary inline" href="<?php print $permalink ; ?>">Register</a>  <a class="button primary inline" href="<?php print $permalink; ?>">Details</a> </p>
            <?php
               print "</div></div><div class=\"clear\"></div></div>\n";       
               $sid=$i+1;
               $i++;
               }
               
                     ?>      
            <?php endforeach; 
               wp_reset_postdata();?>

  </div>
</section>  
 
 <section class="join-mobey-forum">

   <div class="wrapper">
      <h3 class="white caps">Get access to member events</h3>
      
      <p class="hero-buttons"><a class="button primary inline" href="/join/">Join</a></p>
   </div>
</section>  

  
<?php get_footer(); ?>

