<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->


			<!-- header -->
			<header class="header clear">
			<div class="helpcontainer">	
        <div class="wrapper">
           <!-- logo -->
					<div class="logo">
						<a href="<?php echo home_url(); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/mobey-forum-logo-new.png" alt="Mobey Forum" class="logo-img">
						</a>
					</div>
					<!-- /logo -->
										
					<nav class="nav primary_nav_wrap" role="navigation">
						<?php html5blank_nav(); ?>				
					</nav>
					<div class="mobile-nav-buttons">
					<ul>			
					<li><div class="mobile-menu-toggle"></div><li>
					<li class="login-button"><span class="login-icon">Login</span></li>
					<li class="logout-button"><a href="/logout/" class="login-icon">Logout</a></li>
					</ul>
					</div>
					<!-- /nav -->
        
        
        			<div class="search-tooltip">
			<form class="search-front" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-input2" type="search" name="s" placeholder="<?php _e( 'Search Mobey Forum', 'html5blank' ); ?>">
	<button class="search-submit" type="submit" role="button"></button>
	</form>
	</div>
        
        </div><!-- wrapper -->
        </div>
       
			</header>


			
			<section class="mobile-menu">
        <div class="wrapper">
          <nav class="mobile_nan_wrap" role="navigation">
              <?php html5blank_nav(); ?>				
          </nav>
          <div class="mobile-divider"></div>
          <div class="mobile-menu-social-media">
					 						<a href="https://www.facebook.com/MobeyForum"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="facebook" class="facebook-logo"></a>
					 						<a href="http://twitter.com/MobeyForum"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png" alt="facebook" class="twitter-logo"></a>
					 						<a href="http://www.linkedin.com/groups/Mobey-Forum-Mobile-payments-mobile-3441551"><img src="<?php echo get_template_directory_uri(); ?>/img/linkedin.png" alt="facebook" class="linkedin-logo"></a>

				</div>
          Questions? Contact us at<br>
          <a class="mobile-email" href="mailto:&#109;&#111;&#098;&#101;&#121;&#102;&#111;&#114;&#117;&#109;&#064;&#109;&#111;&#098;&#101;&#121;&#102;&#111;&#114;&#117;&#109;&#046;&#111;&#114;&#103;">&#109;&#111;&#098;&#101;&#121;&#102;&#111;&#114;&#117;&#109;&#064;&#109;&#111;&#098;&#101;&#121;&#102;&#111;&#114;&#117;&#109;&#046;&#111;&#114;&#103;</a>
        </div>
			</section>
			<!-- /header -->
