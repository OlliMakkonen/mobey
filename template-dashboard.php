<?php  /* Template Name: Dashboard Page Template */ 

//if (!is_user_logged_in() ) wp_redirect( site_url( 'home' ) );
get_header(); 
 $current_user = wp_get_current_user();
?>
<!-- section -->

<section class="dashboard-header white-text">
   <div class="wrapper">
  <div class="one-third">
  <h1>Member Corner</h1>
  Welcome, <?php print $current_user->user_firstname; ?>
  <a class="dashboard-logout" href="/wp-login.php?action=logout">Logout</a>
  
  </div>
  <div class="two-third">
  <a class="dash-link" href="/members-area/presentations-archive/">presentation archives</a>
  </div>
 <div class="three-third">
   <a class="dash-link" href="/downloads/WG agenda BCN.pdf">Mobey Day Workgroup Agenda</a>
  </div>

       <div class="clear"></div>
   </div>
</section>
<section class="dashboard-items">
   <div class="wrapper">
   
   
  
   
   
   <div class="dashboard-event">
    <div class="dashboard-event-heading">
       Upcoming Event
      <a class="deh-link" href="/events/">See all Events</a>
    </div>


            <?php 
               $indicators = array();
               wp_reset_postdata();  
               $args = array(
               'post_type' => 'Events',
               'posts_per_page' => 4, 
               'orderby' => 'menu_order', 
                 'order' => 'ASC', 
               );
               $i=0;
               $posts_array = get_posts( $args ); 
               foreach ( $posts_array as $post ) : setup_postdata( $post ); 
               
              
               $permalink = get_permalink($post);
               $custom = get_post_custom($post->ID);
               $location= $custom["location"][0]; 
               $members_only= $custom["members_only"][0];
               $past_event= $custom["past_event"][0]; 
               $theme= $custom["theme"][0];
               $headingi= $custom["heading"][0];
               $start_date= $custom["alkaa_pv"][0];
               $end_date= $custom["loppuu_pv"][0];
               $year = substr($alkaa_pv, -4);
               $short_info_text= $custom["short_info_text"][0];
                       $button_1_text = $custom['pb-text'][0];
               $button_1_link = $custom['pb-url'][0];
               $button_2_text = $custom['sb-text'][0];
               $button_2_link = $custom['sb-url'][0];
               
                if ($past_event!=1){
                       
          $fully_booked = $custom["booked"][0];
$indeksi = strtotime($start_date." 15:00:$i");
$indeksi2 = strtotime($end_date." 15:00:$i");          

 
 $date1 = new DateTime();
$date1->setTimestamp($indeksi);

$date2 = new DateTime();
$date2->setTimestamp($indeksi2);

if ($date1->format('Y-m') === $date2->format('Y-m')) {
$reader_friendly = date('jS', $indeksi)." - ".date('jS \o\f F, Y', $indeksi2);
}else {
$reader_friendly = date('jS \o\f M', $indeksi)." - ".date('JS \o\f M, Y', $indeksi2);
} 
               
               
               
               $thumbnail_url = get_the_post_thumbnail_url($post->ID, 'medium');            
               
               //create slide in carousel  
              
               print "<div class=\"featured-event-container-db\">";
               print "<div class=\"featured-event-image-db\" style=\"background-image: url($thumbnail_url);\">";
               print "</div>";
               print "<div class=\"featured-event-content-db\">";   
               print "<div class=\"featured-event-heading-db\">";
               print "<h2 class=\"featured-heading-db\">$location $headingi</h2>";
                              
               print "</div>";  
               print "<div class=\"featured-event-information-db\">";
               print "<div class=\"featured-date\"><div class=\"calendar\"></div> $reader_friendly</div>";
               if ($theme)print "<div class=\"featured-theme\"><div class=\"theme\"></div> $theme</div>";
               print "</div>"; 
               print "<div class=\"featured-short-info\">$short_info_text</div>";
               ?>
      <p class="event-buttons">
           <?php if ($button_1_link){ ?>
            <a class="button primary inline" href="<?php print $button_1_link; ?>"><?php print $button_1_text; ?></a>
            <?php
                  }
                  if ($button_2_link){
                  ?>
            <a class="button secondary inline" href="<?php print $button_2_link ; ?>"><?php print $button_2_text; ?></a> 
            
            <?php
               }
            
               print "</p></div></div></div>";       
                break;
               }     
                     ?>      
            <?php endforeach; 
               wp_reset_postdata();?>
              
         
        <div class="one-third-db-container reports-container-db">
    <div class="dashboard-event-heading">
       Reports
      <a class="deh-link" href="/category/whitepapers">See all Reports</a>
    </div>
    
   <?php
           $i=0;
            $args = array( 'posts_per_page' => 5, 'category' => 98 );
            
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post ); 
            print "<div class=\"news-container\">";
           if( function_exists('mnp_is_new_post')) if (mnp_is_new_post($post->ID)==true)$notification="<div class=\"red-dot\"></div>";
            else $notification="";
            
            $i++;
            ?>
         <div class="heading-div-db"><a class="news-heading size14" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
         <div class="news-date left"><?php print $notification;  print get_the_date('jS \o\f F Y'); ?></div>
         <div class="download-link"><a class="read-more right" href="<?php the_permalink(); ?>">Read more</a></div>
          <div class="clear"></div>
      </div>
      <?php endforeach; 
         wp_reset_postdata();?>
   
   </div>
      
      
        <div class="one-third-db-container webinars-container">
    <div class="dashboard-event-heading">
       Webinars
      <a class="deh-link" href="/category/webinars/">See all webinars</a>
    </div>
    
   <?php
           $i=0;
            $args = array( 'posts_per_page' => 3, 'category' => 149);
            
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post ); 
            $custom = get_post_custom($post->ID);
           $alkaa = $custom["alkaa_pv"][0];
      $alkaa_klo= $custom["alkaa_klo"][0];
      $loppuu= $custom["loppuu_pv"][0];
      $loppuu_klo= $custom["loppuu_klo"][0];
      $registration_link= $custom["registration_link"][0];
       $youtube_id= $custom["paikka"][0];
        $indeksi = strtotime($alkaa." 15:00:$i");
        $today = strtotime("now");
        $pvm = strtotime($alkaa." ".$alkaa_klo);
        $notification="";
        if ($today < $pvm) $future=true; else $future=false;
          
        if ($future == true)  $webinars[$indeksi] .= "<div class=\"news-container future\">";
        else $webinars[$indeksi] .= "<div class=\"news-container \">";
            
         if ($future == true) $webinars[$indeksi] .= '<div class="upcoming-div-db">Upcoming Webinar:</div>';
         $webinars[$indeksi] .= '<div class="heading-div-db"><a class="news-heading size14" href="'.get_the_permalink().'">'.get_the_title().'</a></div>';
          if( function_exists('mnp_is_new_post') && $future == false) if (mnp_is_new_post($post->ID)==true)$notification="<div class=\"red-dot\"></div>";
         if ($future == true) $notification="<div class=\"red-shout\">!</div>";
         $webinars[$indeksi] .= '<div class="news-date left">'.$notification.date('jS \o\f F Y', $pvm).'</div>';
       if ($youtube_id)  $webinars[$indeksi] .= '<div class="download-link"><a class="read-more right" href="'.get_the_permalink().'">Watch now</a></div>';
        else$webinars[$indeksi] .= '<div class="download-link right"><a class="read-more" href="'.get_the_permalink().'">Watch now</a></div>';
         $webinars[$indeksi] .= ' <div class="clear"></div></div>';
      endforeach; 
         wp_reset_postdata();
         $i=0;
       
         krsort($webinars);
         foreach ($webinars as $key => $val) {
         $i++;
          print $val;
          if ($i==2) break;
          
          }
          ?>
   </div>

   
   
   
   

     
      <div class="one-third-db-container blogs-container-db">
       <div class="dashboard-event-heading">
       Blogs
      <a class="deh-link" href="/category/blogs/">See all blogs</a>
    </div>
    
   <?php
           $i=0;
            $args = array( 'posts_per_page' => 3, 'category' => 4 );
            
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post ); 
            print "<div class=\"blog-container\">";
            $i++;
            
              if ( has_post_thumbnail() ) {
          the_post_thumbnail(array(165,216));
          } 
            
            ?>
         <div class="heading-div-db"><a class="news-heading size14" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
         <div class="blogs-content"><?php html5wp_excerpt(10); ?></div>
         <div class="news-date left"><?php print get_the_date('jS \o\f F Y'); ?></div>
         <div class="download-link"><a class="read-more right" href="<?php the_permalink(); ?>">Read more</a></div>
          <div class="clear"></div>
      </div>
      <?php endforeach; 
         wp_reset_postdata();?>
   </div>
   </div>
       <div class="clear"></div>
      </div>
      
</section>

<?php get_footer(); ?>

