

<?php /* Template Name: Frontpage Page Template */ get_header(); ?>
<!-- section -->
<?php $slide_texts =  get_option('slide_settings'); ?>
<section id="frontpage-hero" class="">
   <div class="wrapper white-text align-center">

      <div id="HeroCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
         <div class="carousel-inner">   
           <div class="item active">
            <div class="item-inner"> 
             <div class="content-absolute">
           <?php
           if (filter_var($slide_texts['hero_text'], FILTER_VALIDATE_URL)) {
           ?>
           <img src="<?php print $slide_texts['hero_text']; ?>" alt="Mobey Forum slide" class="center-margin">
           <?php } else { ?>
              <h1><?php print $slide_texts['hero_heading']; ?></h1>
              <p class="bigtext hero-text"><?php print $slide_texts['hero_text']; ?></p>
               <?php } ?>
              <p class="hero-buttons">
                          <?php if (!empty($slide_texts['button_1_link'])) { ?> <a class="button primary inline" href="<?php print $slide_texts['button_1_link']; ?>"><?php print $slide_texts['button_1_text']; ?></a><?php } ?>
              <?php if (!empty($slide_texts['button_2_link'])) { ?>  <a class="button secondary inline" href="<?php print $slide_texts['button_2_link']; ?>"><?php print $slide_texts['button_2_text']; ?></a><?php } ?>

               </p>
               </div>
               <img class="slider-image" src="<?php echo get_template_directory_uri(); ?>/img/hero-content.jpg" alt="hero-image">
               
              </div>
             </div>  
            
          
            <div class="item">  
            <div class="item-inner"> 
              <div class="content-absolute">
                 <?php
           if (filter_var($slide_texts['hero_text2'], FILTER_VALIDATE_URL)) {
           ?>
           <img src="<?php print $slide_texts['hero_text2']; ?>" alt="Mobey Forum slide" class="center-margin">
           <?php } else { ?>
              <h1><?php print $slide_texts['hero_heading2']; ?></h1>
              <!--<p class="bigtext hero-text"><?php print $slide_texts['hero_text2']; ?></p>-->
               <?php } ?>
               <div class="being-part-container carousel-pbc">
        <div class="bpc bpc1">
        meet industry friends in an <b>open and honest environment</b>
        </div>
        <div class="bpc-divider"></div>        
        <div class="bpc">
        have access to <b>expert brain power</b>
        </div>
        <div class="bpc-divider"></div>
        <div class="bpc">
        are a <b>core asset</b> in content creation
        </div>
        <div class="bpc-divider"></div>
        <div class="bpc bpc2">
        are the platform for recognizing and <b>leading future challenges</b> and trends
        </div>
        <div class="clear"></div>
      </div> 
              <p class="hero-buttons">
             <?php if (!empty($slide_texts['button_1_link2'])) { ?> <a class="button primary inline" href="<?php print $slide_texts['button_1_link2']; ?>"><?php print $slide_texts['button_1_text2']; ?></a><?php } ?>
              <?php if (!empty($slide_texts['button_2_link2'])) { ?>  <a class="button secondary inline" href="<?php print $slide_texts['button_2_link2']; ?>"><?php print $slide_texts['button_2_text2']; ?></a><?php } ?>
              </p>
                  </div>
               <img class="slider-image" src="<?php echo get_template_directory_uri(); ?>/img/hero-members.jpg" alt="hero-image">
               
            </div> 
             </div>   
            
            <div class="item">  
             <div class="item-inner"> 
               <div class="content-absolute">
                 <?php
           if (filter_var($slide_texts['hero_text3'], FILTER_VALIDATE_URL)) {
           ?>
           <img src="<?php print $slide_texts['hero_text3']; ?>" alt="Mobey Forum slide" class="center-margin">
           <?php } else { ?>
              <h1><?php print $slide_texts['hero_heading3']; ?></h1>
              <p class="bigtext hero-text"><?php print $slide_texts['hero_text3']; ?></p>
               <?php } ?>
              <p class="hero-buttons">
             <?php if (!empty($slide_texts['button_1_link3'])) { ?> <a class="button primary inline" href="<?php print $slide_texts['button_1_link3']; ?>"><?php print $slide_texts['button_1_text3']; ?></a><?php } ?>
              <?php if (!empty($slide_texts['button_2_link3'])) { ?>  <a class="button secondary inline" href="<?php print $slide_texts['button_2_link3']; ?>"><?php print $slide_texts['button_2_text3']; ?></a><?php } ?>
              </p>
                  </div>
               <img class="slider-image" src="<?php echo get_template_directory_uri(); ?>/img/hero-events.jpg" alt="hero-image">
               
              
            </div>
             </div>  
             
             <div class="item">  
             <div class="item-inner"> 
                <div class="content-absolute">
                 <?php
           if (filter_var($slide_texts['hero_text4'], FILTER_VALIDATE_URL)) {
           ?>
           <img src="<?php print $slide_texts['hero_text4']; ?>" alt="Mobey Forum slide" class="center-margin">
           <?php } else { ?>
              <h1><?php print $slide_texts['hero_heading4']; ?></h1>
              <p class="bigtext hero-text"><?php print $slide_texts['hero_text4']; ?></p>
               <?php } ?>
              <p class="hero-buttons">
             <?php if (!empty($slide_texts['button_1_link4'])) { ?> <a class="button primary inline" href="<?php print $slide_texts['button_1_link4']; ?>"><?php print $slide_texts['button_1_text4']; ?></a><?php } ?>
              <?php if (!empty($slide_texts['button_2_link4'])) { ?>  <a class="button secondary inline" href="<?php print $slide_texts['button_2_link4']; ?>"><?php print $slide_texts['button_2_text4']; ?></a><?php } ?>
              </p>
                  </div>
               <img class="slider-image" src="<?php echo get_template_directory_uri(); ?>/img/hero-extra1.jpg" alt="hero-image">
               
            </div>
             </div>   
              
      <div class="clear"></div>
         <div class="carousel-indicators hero-indicators">
           <div class="hero-indicator active" data-target="#HeroCarousel" data-slide-to="0">
           <div class="indicator-button">
           <?php print $slide_texts['slide_button1']; ?>
           </div>
           <div class="indicator-dot"></div>
           </div>
           <div class="hero-indicator" data-target="#HeroCarousel" data-slide-to="1">
           <div class="indicator-button">
          <?php print $slide_texts['slide_button2']; ?>
          </div>
          <div class="indicator-dot"></div>
           </div>
           <div class="hero-indicator" data-target="#HeroCarousel" data-slide-to="2">
           <div class="indicator-button">
          <?php print $slide_texts['slide_button3']; ?>
          </div>
          <div class="indicator-dot"></div>
           </div>
           <div class="hero-indicator" data-target="#HeroCarousel" data-slide-to="3">
           <div class="indicator-button">
          <?php print $slide_texts['slide_button4']; ?>
          </div>
          <div class="indicator-dot"></div>
           </div>
           
          <div class="arrow-wrapper">
          <a class="left carousel-control" href="#HeroCarousel" data-slide="prev"></a>
          <a class="right carousel-control" href="#HeroCarousel" data-slide="next"></a>
          </div> 
           
       </div>
     
       
       
         </div>
      </div>
      <div class="mobey-day-diamond">
   <img src="<?php echo get_template_directory_uri(); ?>/img/mobey-forum-diamond-logo@2x.png" alt="Mobey Forum" class="">
      </div>
   </div>
</section>
<!-- /section -->

<!-- section -->
<section class="maintext">
   <div class="wrapper">
  
      <?php if (have_posts()): while (have_posts()) : the_post(); ?>
      <!-- article -->
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
         <?php the_content(); ?>
         <br class="clear">
      </article>
      <!-- /article -->
      <?php endwhile; ?>
      <?php endif; ?>
      
      
      
   </div>
</section>
<!-- /section -->
<section class="join-mobey-forum">
   <div class="wrapper">
      <h3 class="white caps">Join Mobey Forum</h3>
      <p class="hero-buttons"><a class="button primary inline" href="/join/">Join</a></p>
   </div>
</section>
<section class="mobey-events">
   <div class="wrapper">
      <div class="center size-half">
         <h2 class="">Mobey Events</h2>
         <p class="size20">Atmosphere at our events is upfront and trustful. Our events are liked for open conversations, trustful collaboration and networking. We often create insights that money cannot buy.</p>
      </div>
      <!-- Wrapper for slides -->
      <div id="EventCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
         <div class="carousel-inner">
           
            <?php 
               $indicators = array();
               wp_reset_postdata();  
               $args = array(
               'post_type' => 'Events',
               'posts_per_page' => 4, 
                'orderby' => 'menu_order', 
                 'order' => 'ASC', 
               );
               $i=0;
               $active_printed=false;
               $posts_array = get_posts( $args ); 
               foreach ( $posts_array as $post ) : setup_postdata( $post ); 
               
             //  $post = $posts_array[$i];
               $permalink = get_permalink($post);
               $custom = get_post_custom($post->ID);
               $location= $custom["location"][0]; 
               $members_only= $custom["members_only"][0]; 
               $past_event= $custom["past_event"][0];
               $theme= $custom["theme"][0];
               $headingi= $custom["heading"][0];
               $start_date= $custom["alkaa_pv"][0];
               $end_date= $custom["loppuu_pv"][0];
               $button_1_text = $custom['pb-text'][0];
               $button_1_link = $custom['pb-url'][0];
               $button_2_text = $custom['sb-text'][0];
               $button_2_link = $custom['sb-url'][0];
               
               $year = substr($alkaa_pv, -4);
               $short_info_text= $custom["short_info_text"][0];
                if ($past_event==0 &&  $active_printed==false){$active = " active"; $active_printed=true;} else $active = "";
                       
          $fully_booked = $custom["booked"][0];
$indeksi = strtotime($start_date." 15:00:$i");
$indeksi2 = strtotime($end_date." 15:00:$i");          

 
 $date1 = new DateTime();
$date1->setTimestamp($indeksi);

$date2 = new DateTime();
$date2->setTimestamp($indeksi2);

if ($date1->format('Y-m') === $date2->format('Y-m')) {
$reader_friendly = date('jS', $indeksi)." - ".date('jS \o\f F, Y', $indeksi2);
}else {
$reader_friendly = date('jS \o\f M', $indeksi)." - ".date('JS \o\f M, Y', $indeksi2);
} 
               
               
               
               $thumbnail_url = get_the_post_thumbnail_url($post->ID, 'large');
               $thumbnail_url2 = get_the_post_thumbnail_url($post->ID, 'medium');
               
               
               //create slide in carousel  
              print "<div class=\"item $active\">\n";
;
               print "<div class=\"featured-event-container\">";
               print "<div class=\"featured-event-image\" style=\"background-image: url($thumbnail_url);\">";
               print '<img src="'.get_template_directory_uri().'/img/event-image-mask.png" alt="mobey" class="event-image-mask">';
               print "</div>";
               print "<div class=\"featured-event-content\">";   
               print "<div class=\"featured-event-heading-no-diamond\">\n";
               print "<h2 class=\"featured-heading\">$location</h2>\n";
               print "<h3 class=\"featured-sub-heading\">$headingi</h3>\n";
               if ($members_only == 1)print "<div class=\"members-only-diamond\">Members<br> only</div>";
               print "</div>";  
               print "<div class=\"featured-event-information\">";
               if ($theme)print "<div class=\"featured-theme\"><div class=\"theme\"></div> $theme</div><br>\n";
               print "<div class=\"featured-date\"><div class=\"calendar\"></div> $reader_friendly</div>";
               print "<div class=\"featured-short-info\">$short_info_text</div></div>\n";
               ?>
               <p class="event-buttons">
           <?php if ($button_1_link){ ?>
            <a class="button primary inline" href="<?php print $button_1_link; ?>"><?php print $button_1_text; ?></a>
            <?php
                  }
                  if ($button_2_link){
                  ?>
            <a class="button secondary inline" href="<?php print $button_2_link ; ?>"><?php print $button_2_text; ?></a> 
            
            <?php
               }
               
            
               print "</p></div><div class='clear'></div></div></div>\n";       
               $sid=$i;
               //create the indicator
               $indicators[$i] =   "<div class=\"events-cell $active\" data-target=\"#EventCarousel\" data-slide-to=\"$sid\" style=\"background-image: url($thumbnail_url2);\">";
               $indicators[$i] .= '<div class="passive-border"></div><div class="passive-indicator"><p>';
               $indicators[$i] .= $location;
               $indicators[$i] .=  "<br><span class=\"size26 bold\">$headingi</span></p>";
               $indicators[$i] .= "<div class=\"size14 bold\">$reader_friendly</div></div></div>";
                       $i++;
                     ?>      
            <?php endforeach; 
               wp_reset_postdata();?>
         </div>
         <div class="clear"></div>
         <div class="events-row carousel-indicators">
            <?php
               foreach ($indicators as $indicator) {
                  echo $indicator;
               }
                ?>
               
         </div>
       <div class="clear"></div>
      </div>
      <div class="gain-access-container">
        <div class="gain-access size26">
        <!--<div class="diamond-left">Members<br> Only</div>
        <div class="diamond-right">Members<br> Only</div>-->
           <p class="bold">Gain access to members 
              events by joining us!
           </p>
          
        </div>
         <div clasS="button-absolute"><a class="button primary inline" href="/join/">Join</a></div>
      </div>
   </div>
</section>
<section class="workgroups">
   <div class="wrapper">
      <h2 class="center">Workgroups</h2><br>
      <div class="workgroup-container">
         <div class="workgroup-cell-50">
           <?php print $slide_texts['workgroups_upper_text']; ?>
           
                    </div>
         <div class="workgroup-cell-50-2">
         <div class="wc-line-y"></div>
         <div class="wc-line-x"></div>
              <div class="banking-table">
                <div class="banking-table-row">
                  <div class="banking-cell1"><?php print $slide_texts['theme_1_text']; ?></div>
                  <div class="banking-cell2"><?php print $slide_texts['theme_2_text']; ?></div>
                </div>    
                <div class="banking-table-row">
                  <div class="banking-cell3"><?php print $slide_texts['theme_3_text']; ?></div>
                  <div class="banking-cell4"><?php print $slide_texts['theme_4_text']; ?></div>
                </div>  
              </div> 
          <p><a class="button primary inline" href="<?php print $slide_texts['themebutton_link']; ?>"><?php print $slide_texts['themebutton_text']; ?></a></p>
         </div>
      <div class="workgroup-cell-60 white-text">
      <?php print $slide_texts['workgroups_lower_text']; ?>
      </div>
       <div class="workgroup-cell-40">
       
     
      <?php
      $i=1;
         $args = array( 'posts_per_page' => 2, 'category' => 98 );
         
         $myposts = get_posts( $args );
         foreach ( $myposts as $post ) : setup_postdata( $post ); 
         print "<div class=\"div$i reports-container\">";
         $i++;
         
                  if (str_word_count (get_the_title())<5)$long_excerpt=true; else $long_excerpt=false;
         if (str_word_count (get_the_title())>10)$no_content=true; else $no_content=false;

          ?>
        <div class="report-indicator">report</div>
        <div class=" blog-heading-div"><a class="news-heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
        <?php if ($long_excerpt==true) { ?> <div class="blogs-content"><?php html5wp_excerpt(18); ?></div> <?php } ?>
        <?php if ($long_excerpt==false && $no_content==false) { ?> <div class="blogs-content"><?php html5wp_excerpt(10); ?></div> <?php } ?>

        
        <div class="blogs-date"><?php print get_the_date('jS \o\f F Y'); ?>
              <div class="blog-link"><a class="read-more" href="<?php the_permalink(); ?>">Read more</a></div>

      </div>
      </div>
   
   <?php endforeach; 
      wp_reset_postdata();?>
   </div>
   <div class="clear"></div>
      </div>
      
      
      
      </div>
   <div class="clear"></div>
 
</section>
<section class="press-and-blog">
   <div class="wrapper">
      <div class="press-releases">
         <h2>Press Releases</h2>
         <?php
           $i=0;
            $args = array( 'posts_per_page' => 3, 'category' => 7 );
            
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post ); 
            print "<div class=\"div$i news-container\">";
            $i++;
            ?>
         <div class="heading-div"><a class="news-heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
         <div class="news-date"><?php print get_the_date('jS \o\f F Y'); ?></div>
         <div class="news-link"><a class="read-more" href="<?php the_permalink(); ?>">Read more</a></div>
      </div>
      <?php endforeach; 
         wp_reset_postdata();?>
   </div>
   <div class="blogs">
      <h2>Blogs</h2>
      <?php
      $i=1;
         $args = array( 'posts_per_page' => 2, 'category' => 4 );
         
         $myposts = get_posts( $args );
         foreach ( $myposts as $post ) : setup_postdata( $post ); 
         print "<div class=\"div$i blogs-container\">";
         $i++;
         
         if ( has_post_thumbnail() ) {
          print "<div class=\"blog-thumb\">";
          the_post_thumbnail(array(165,216));
           print "</div>";
          } 
          ?>
        <div class="blog-content-right">  
          <div class="blog-indicator">blog</div>
          <div class=" blog-heading-div"><a class="news-heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
          <div class="blogs-author"><a href="/category/blog/">Mobey Forum's Blog</a></div>
          <div class="blogs-content"><?php html5wp_excerpt(); ?></div>
          <div class="blogs-date"><?php print get_the_date('jS \o\f F Y'); ?>
          <div class="blog-link"><a class="read-more" href="<?php the_permalink(); ?>">Read more</a></div>
          </div>
      </div>
   </div>
   <div class="clear"></div>
   <?php endforeach; 
      wp_reset_postdata();?>
   </div>
   <div class="clear"></div>
   </div> 
   
</section>
<section class="twitter-feed">
   <div class="wrapper center">
      <h2>Twitter feed</h2>
        
     <?php echo do_shortcode("[custom-twitter-feeds]"); ?>
   </div>
   <div class="clear"></div>
</section>
<?php get_footer(); ?>

